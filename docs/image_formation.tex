\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage[margin=1in,paperwidth=8.5in,paperheight=11in]{geometry}
\usepackage{float}
\usepackage{subfig}
\usepackage{enumerate}
\usepackage{enumitem}

\allowdisplaybreaks

% Generally useful commands/definititions

\DeclareMathOperator{\diag}{diag} 
\DeclareMathOperator{\trace}{\mathbf{Tr}}
\DeclareMathOperator{\grad}{\nabla}
\DeclareMathOperator{\approaches}{\rightarrow}
\newcommand{\innerprod}[2]{\langle #1,#2 \rangle}
\newcommand{\symmat}{\mathcal{S}}

\newcommand{\minimize}[1]{\underset{#1}{\text{minimize}}\ }
\newcommand{\maximize}[1]{\underset{#1}{\text{maximize}}\ }
\renewcommand{\min}[1]{\underset{#1}{\text{min}}\ }
\renewcommand{\max}[1]{\underset{#1}{\text{max}}\ }
\newcommand{\subjectto}{\text{subject to\ }}

\begin{document}

\begin{center}
    {\LARGE{\bf Image Formation}}
\end{center}

\section{Pinhole-camera model}

We first consider the simplified camera model where the homogeneous transform
from a point in world coordinates to pixel coordinates is given by the
multiplication by the extrinsics matrix $\boldsymbol{E}$ followed by the camera
matrix $\boldsymbol{K}$. We assume pixels are square and that the image origin
is at $(W/2, H/2)$, where $W$ and $H$ are the image width and height,
respectively.

\subsection{1D camera geometry}

In the pinhole-camera model, the projected point $u$ in the image plane of the
world point $(x, y)$ is given by the homogeneous equation
\begin{equation}
    \tilde{u} = \boldsymbol{K}\boldsymbol{E}
    \left[\begin{array}{c}x\\y\\1\end{array}\right],
\end{equation}
where
\begin{align}
    \boldsymbol{K} = \left[\begin{array}{ccc}0 & f & 0\\
                                             1 & 0 & 0\end{array}\right],
    \boldsymbol{E} = \left[\begin{array}{cc}\boldsymbol{R}_{2 \times 2} &
                                            \boldsymbol{t}_{2 \times 1}\\
                                            \boldsymbol{0}^\top & 1
                      \end{array}\right],
\end{align}
and $f$ represents the focal length and $\boldsymbol{R}$ and $\boldsymbol{t}$
represent the rotation matrix and translation vector which transform the point
from the world frame to the camera frame. This assumes the reference frame
of $\theta = 0$ has the camera pointing right.
Note: If the rotation and translation
of the camera in the world frame are given by $\boldsymbol{R}_{c}$ and
$\boldsymbol{t}_{c}$, then $\boldsymbol{R} = \boldsymbol{R}_{c}^{-1}$ and
$\boldsymbol{t} = -\boldsymbol{R}\times\boldsymbol{t}_{c}$.

\subsection{2D camera geometry}

Similar to the 1D case, the projected point $p = (u, v)$ in the image plane is
given by
\begin{equation}
    \tilde{p} = \boldsymbol{K}\boldsymbol{E}\left[
        \begin{array}{c}x\\y\\z\\1\end{array}\right]
\end{equation}
where
\begin{align}
    \boldsymbol{K} &= \left[\begin{array}{ccc}f & 0 & 0\\0 & f & 0\\0 & 0 & 1
        \end{array}\right],\\
    \boldsymbol{E} &= \left[\begin{array}{cc}\boldsymbol{R}_{3 \times 3} &
        \boldsymbol{t}_{3 \times 1}\\
        \boldsymbol{0}^\top & 1\end{array}\right].
\end{align}

\section{Extensions to the model}

The above models assume a simple linear model, which we augment below to make
more realistic.

\subsection{Radial lens distortion}

We use a simple quadratic distortion model with distortion parameters
$\kappa_1$ and $\kappa_2$. Consider the 2D camera case; the 1D case is exactly
the same except without $y$. Let $(x_c, y_c)$ be the coordinates obtained
after division by the third coordinate but before scaling by $f$, and let
$r = \sqrt{x_c^2 + y_c^2}$. Applying lens distortion,
\begin{align}
\hat{x}_c &= x_c(1 + \kappa_1 r^2 + \kappa_2 r^4)\\
\hat{y}_c &= y_c(1 + \kappa_1 r^2 + \kappa_2 r^4),
\end{align}
and the final image coordinates are obtained after scaling by $f$.

\subsection{Depth of field}

The lens equation
\begin{equation}\label{eq:lens}
    \frac{1}{u} + \frac{1}{v} = \frac{1}{f},
\end{equation}
where $u = $ object distance from focal point, $v =$ image distance from
focal point, and $f = $ focal length
must be satisfied for a point to be in perfect focus. Otherwise, suppose the
camera is focused on a depth $Z_0$. Let $d$ be the diameter of the camera
aperture. Points at depth $Z$ will have blur circles (or "circles of confusion"
) with diameter
\begin{equation}
    \frac{d(z^* - z_0^*)}{z^*},
\end{equation}
where $z^*$ and $z_0^*$ are the optimal image distances as given by
(\ref{eq:lens}).

\section{References}

%TODO Bibtex these
%<a href="http://www.vision.caltech.edu/bouguetj/calib_doc/htmls/parameters.html">Bouguet Camera Calibration Toolbox</a><br />
%<a href="http://www.eecs.berkeley.edu/~trevor/CS280Notes/">Darrell CS 280 slides</a><br />
%<a href="http://szeliski.org/Book/">Szeliski Computer Vision: Algorithms and Applications</a><br />
%<a href="http://en.wikipedia.org/wiki/Circle_of_confusion">Wikipedia article on "circle of confusion"</a>
%Jitendra Malik's lecture notes for CS280

\end{document}
