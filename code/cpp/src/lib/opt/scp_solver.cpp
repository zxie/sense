#include "scp_solver.h"
#include "gurobi_solver.h"
#include "timer.h"

void scp_solver(Robot &r, const vector<VectorXd>& X_bar, const vector<VectorXd>& U_bar, const vector<MatrixXd>& W_bar, const double rho_x, const double rho_u, const VectorXd& x_goal, const int N_iter, vector<VectorXd>& opt_X, vector<VectorXd>& opt_U, MatrixXd& opt_K, VectorXd& opt_u0) { 

  int NX = X_bar[0].rows();
  int NU = U_bar[0].rows();
  int T = U_bar.size();
  assert(T+1 == X_bar.size());

  vector<VectorXd> X_scp(T+1), U_scp(T); 
  for (int i = 0; i < T+1; i++) 
    X_scp[i] = X_bar[i];
  for (int i = 0; i < T; i++)
    U_scp[i] = U_bar[i]; 

  Timer timer = Timer();
  for (int iter = 0; iter < N_iter; iter++) 
  {

    // Compute Jacobians
    vector<MatrixXd> As(T), Bs(T);
    vector<VectorXd> Cs(T);
    r.db_trajectory(X_scp, U_scp, As, Bs, Cs); //TODO: FIX HACK
    
    // Setup variables for sending to convex solver
    vector<VectorXd> opt_X, opt_U;
    //SparseMatrix<double> opt_Q;
    //VectorXd opt_r;

    //Send to convex solver
    convex_gurobi_solver(X_scp, U_scp, W_bar, As, Bs, Cs, rho_x, rho_u, x_goal, opt_X, opt_U, opt_K, opt_u0, iter == (N_iter - 1));

    //Shoot
    
    for (int t = 0; t < T; t++) {
      U_scp[t] = opt_U[t]; 
      r.belief_dynamics(X_scp[t], U_scp[t], X_scp[t+1]);
    }

    // Collocation
    //X_scp = opt_X;
    //U_scp = opt_U;

    //if (iter < N_iter - 1) // hack for now  
    //  X_scp[T] = x_goal;
    cout << "Iter " << iter << " time: " << timer.elapsed() << endl; 
  }

  opt_X = X_scp;
  opt_U = U_scp; 


}

