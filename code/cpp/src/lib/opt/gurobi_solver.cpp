#include "gurobi_solver.h"

void A_prod(const vector<MatrixXd> &As, int tau, int t, MatrixXd& result) {
  int NX = As[0].cols(); 
  result = MatrixXd::Identity(NX,NX);
  for (int i = tau; i < t; i++) {
    result = As[i] * result; 
  }
  //cout << "Aprod tau, t = " << tau << ", " << t << endl;
  //cout << result << endl; ; 
}

void build_G(const vector<MatrixXd> &As, SparseMatrix<double> &G) {
  int T = As.size(); 
  int NX = As[0].cols();

  //cout << "building G" << endl; 
  G = SparseMatrix<double>(NX*(T+1),0);
  for (int col = 0; col < T; col++) {
    SparseMatrix<double,RowMajor> column(0,NX);
    SparseMatrix<double> block(NX,NX); 
    for (int row = 0; row < T+1; row++) {
      if (row > col) {
        if (row == col+1)
          block = speye(NX);
        else
          block = toSparse(As[row-1]) * block; 
      
      }
      SparseMatrix<double,RowMajor> tmp;
      vcat(column, block, tmp);
      column = tmp; 
    }
    SparseMatrix<double> tmp2;
    hcat(G, column, tmp2);
    G = tmp2;
  }
  //cout << "building complete" << endl;
}

void build_G_T1(const vector<MatrixXd> &As, SparseMatrix<double> &G_T1) {
  int T = As.size();
  int NX = As[0].cols();

  G_T1 = SparseMatrix<double>(NX,0);
  SparseMatrix<double> block_A;  
  for (int i = T; i > 0; i--) {
    if (i == T) block_A = speye(NX);
    else block_A = block_A * toSparse(As[i]);
    G_T1 = hcat(block_A, G_T1);
  }

}

void build_H(const vector<MatrixXd> &As, const vector<MatrixXd> &Bs, SparseMatrix<double> &H) {
  int T = As.size(); 
  int NX = As[0].cols();
  int NU = Bs[0].cols(); 

  //cout << "building H" << endl; 
  H = SparseMatrix<double>(NX*(T+1),0);
  for (int col = 0; col < T; col++) {
    SparseMatrix<double,RowMajor> column(0,NU);
    SparseMatrix<double> block_A(NX,NU);
    for (int row = 0; row < T+1; row++) {
      SparseMatrix<double> block(NX,NU); 
      if (row > col) { 
        if (row == col+1)
          block_A = speye(NX);
        else
          block_A = toSparse(As[row-1]) * block_A; 
        block = block_A * toSparse(Bs[col]); 
      }
      SparseMatrix<double,RowMajor> tmp;
      vcat(column, block, tmp);
      column = tmp; 
    }
    SparseMatrix<double> tmp2;
    hcat(H, column, tmp2);
    H = tmp2;
  }
  //cout << "building complete" << endl;
}

void build_H_T1(const vector<MatrixXd> &As, const vector<MatrixXd> &Bs, SparseMatrix<double> &H_T1) {
  int T = As.size();
  int NX = As[0].cols();

  H_T1 = SparseMatrix<double>(NX,0);
  SparseMatrix<double> block_A;  
  for (int i = T; i > 0; i--) {
    if (i == T) block_A = speye(NX);
    else block_A = block_A * toSparse(As[i]);
    H_T1 = hcat(block_A * toSparse(Bs[i-1]), H_T1);
  }

}
void build_x0(const vector<MatrixXd> &As, const vector<VectorXd> &Cs, const VectorXd &x_start, const SparseMatrix<double> &G, VectorXd &x0) {
  int T = As.size();
  int NX = As[0].cols(); 
  x0 = VectorXd(NX*(T+1));

  VectorXd c_vec(NX*T);
  for (int t = 0; t < T; t++) {  
    c_vec.segment(t*NX, NX) = Cs[t]; 
  }

  for (int t = 0; t < T+1; t++) {
    MatrixXd A_tau_t;
    A_prod(As, 0, t, A_tau_t);
    x0.segment(t*NX, NX) = A_tau_t * x_start; 
  }

  x0 = x0 + G*c_vec; 

}

void build_kx(int NU, VectorXd& x0, SparseMatrix<double> &result) {
  SparseMatrix<double> sparse_x0 = toSparse(x0.transpose());
  result = SparseMatrix<double>(0,0);
  for (int i = 0; i < NU; i++) { 
    SparseMatrix<double> tmp; 
    blkdiag(result, sparse_x0, tmp);
    result = tmp; 
  }
}

void build_Qx(VectorXd x0, int NX, int NU, int T, SparseMatrix<double> &result) {
  
  result = SparseMatrix<double>(0,0);
  for (int t = 0; t < T; t++) {
    SparseMatrix<double> block;
    VectorXd x0_t = x0.segment(NX*t, NX); 
    build_kx(NU, x0_t, block); 
    SparseMatrix<double> tmp;
    blkdiag(result, block, tmp);
    result = tmp; 
  }
}

void build_xbar(const vector<VectorXd>& X_bar, VectorXd& x_bar) {
  int T1 = X_bar.size();
  int NX = X_bar[0].rows();
  x_bar = VectorXd(T1*NX);
  for (int t = 0; t < T1; t++) {
    x_bar.segment(t*NX, NX) = X_bar[t]; 
  }
}

void build_ubar(const vector<VectorXd>& U_bar, VectorXd& u_bar) {
  int T = U_bar.size();
  int NU = U_bar[0].rows();
  u_bar = VectorXd(T*NU);
  for (int t = 0; t < T; t++) {
    u_bar.segment(t*NU,NU) = U_bar[t];
  } 
}

void build_sample_matrix(const vector<MatrixXd> &W_bar, MatrixXd & W_s) {
  int T = W_bar.size();
  int NS = W_bar[0].cols();
  int NX = W_bar[0].rows(); 

  W_s = MatrixXd(NX*T, NS);  
  for (int t = 0; t < T; t++) {
    for (int s = 0; s < NS; s++) {
      W_s.block(t*NX,s,NX,1) = W_bar[t].col(s); 
    }
  }
}


void convex_gurobi_solver(const vector<VectorXd>& X_bar, const vector<VectorXd>& U_bar, const vector<MatrixXd>& W_bar, const vector<MatrixXd>& As, const vector<MatrixXd>& Bs, const vector<VectorXd>& Cs, const double rho_x, const double rho_u, const VectorXd &x_goal, vector<VectorXd>& opt_X, vector<VectorXd>& opt_U, MatrixXd &opt_K, VectorXd & opt_u0, const bool compute_policy) {

  int NX = X_bar[0].rows(); 
  int NU = U_bar[0].rows();
  int NS = W_bar[0].cols(); 
  int T = U_bar.size(); 

  //cout << "NX = " << NX << endl;
  //cout << "NU = " << NU << endl; 
  //cout << "NS = " << NS << endl;
  //cout << "T  = " << T  << endl; 

  // Construct major matrices
  SparseMatrix<double> G, H;
  SparseMatrix<double> G_T1, H_T1;
  VectorXd x0, x_bar, u_bar, x0_T1;
  MatrixXd W_s; 

  Timer timer = Timer();
  //cout << "building matrices" << endl; 
  build_G(As, G);
  build_G_T1(As, G_T1); 
  build_H(As, Bs, H); 
  build_H_T1(As, Bs, H_T1);
  build_x0(As, Cs, X_bar[0], G, x0);
  build_xbar(X_bar, x_bar);
  build_ubar(U_bar, u_bar);
  build_sample_matrix(W_bar, W_s); 
  x0_T1 = x0.segment(NX*T, NX); 
  //cout << "Time to build:" << timer.elapsed() << endl;
  timer.restart();

  //cout << "Building constraints" << endl; 
  
  /* Formulate as a standard Quadratic Program 
   *
   * variables [x; u; r; q_vec; goal_diff; sample_diff; sample_u] 
   * x -> NX*(T+1) (represents the difference from a nominal trajectory X_bar)
   * u -> NU*T
   * r -> NU*T
   * q_vec -> NX*NU*T
   * x_diff -> NX
   * sample_diff -> NX*NS
   * sample_u -> NU*NS*T 
   */

  int ALL_VARS = NX*(T+1) + NU*T + NU*T + NX*NU*T + NX + NX*NS + NU*NS*T;

  /* Build constraint matrices */

  /* 
   * CONSTRAINT 1 
   * x - x_bar = H*u + x0 - x_bar -> x - H*u = x0 - x_bar
   */
  SparseMatrix<double> x_A;
  VectorXd x_b(NX*(T+1));
  hcat(speye(NX*(T+1)), -H, x_A); // X, U
  x_A = hcat(x_A, spzeros(NX*(T+1), NU*T + NX*NU*T + NX 
        + NX*NS + NU*NS*T )); // r, q, goal_diff, sample_diff, sample_u
  //x_A = tmp;
  x_b = x0 - x_bar; 

  /*
   * CONSTRAINT 2
   * u = Q*x0 + r -> u - r - Q*x0 = 0
   */
  SparseMatrix<double> u_A;
  VectorXd u_b(NU*T);
  
  hcat(spzeros(NU*T, NX*(T+1)), speye(NU*T), u_A);  // X, U
  SparseMatrix<double> Qx0;
  build_Qx(x0, NX, NU, T, Qx0);
  u_A = hcat(u_A, -1*speye(NU*T)); // r
  u_A = hcat(u_A, -Qx0); // q
  u_A = hcat(u_A, spzeros(NU*T, NX + NX*NS + NU*NS*T)); // goal_diff, sample_diff, sample_u
  u_b = VectorXd::Zero(NU*T);
 
  /*
   * CONSTRAINT 3
   * goal_diff = x_T + x_bar_T - x_goal -> x_T - goal_diff = x_goal - x_bar_T
   */

  SparseMatrix<double> g_A;
  VectorXd g_b(NX);
  hcat(spzeros(NX, NX*T), speye(NX), g_A); // X
  g_A = hcat(g_A, spzeros(NX, NU*T + NU*T + NX*NU*T)); // U, r, q
  g_A = hcat(g_A, -speye(NX)); // goal_diff
  g_A = hcat(g_A, spzeros(NX, NX*NS + NU*T*NS)); // sample_diff, sample_u
  g_b = x_goal - X_bar[T]; 

  /*
   * CONSTRAINT 4
   * sample_diff = x_s - x_goal = H_t1 u_s + x0 + G_T1 w_s - x_goal
   *      -> 
   * sample_diff - H_t1 u_s = x0 + G_T1 w_s - x_goal
   */

  SparseMatrix<double> sd_A;
  VectorXd sd_b(NX*NS);
  hcat(spzeros(NX*NS, NX*(T+1) + NU*T + NU*T + NX*NU*T + NX) , speye(NX*NS), 
      sd_A); // x, u, r, q, goal_diff, sample_diff
  SparseMatrix<double> sd_H;
  for (int i = 0; i < NS; i++) sd_H = blkdiag(-H_T1, sd_H);
  sd_A = hcat(sd_A, sd_H); // sample_u 
  for (int i = 0; i < NS; i++) 
    sd_b.segment(i*NX, NX) = x0_T1 + G_T1*W_s.col(i) - x_goal;


  /*
   * CONSTRAINT 5
   * sample_u = Q(x0+G*w_s) + r -> sample_u - Q(x0 + G*w_s) - r = 0
   */

  SparseMatrix<double> su_A;
  VectorXd su_b(NU*T*NS);
  
  SparseMatrix<double> su_A_Q_block(0,NU*NX*T); 
  SparseMatrix<double> su_A_r_block(0,NU*T); 
  for (int i = 0; i < NS; i++) {  
    SparseMatrix<double> Qx0Gws;
    build_Qx(x0 + G*W_s.col(i), NX, NU, T, Qx0Gws);
    su_A_Q_block = vcat(su_A_Q_block, -Qx0Gws);
    su_A_r_block = vcat(su_A_r_block, -speye(NU*T)); 
  }
  hcat(spzeros(NU*T*NS, NX*(T+1) + NU*T), su_A_r_block, su_A);  // X, U, r
  su_A = hcat(su_A, su_A_Q_block); // q
  su_A = hcat(su_A, spzeros(NU*T*NS, NX + NX*NS)); // goal_diff, sample_diff
  su_A = hcat(su_A, speye(NU*T*NS)); // sample_u

  su_b = VectorXd::Zero(NU*T*NS);


  // Concatenate constraints 
  SparseMatrix<double,RowMajor> A;
  vcat(x_A, u_A, A); // Constraint 1, 2
  A = vcat(A, g_A); // Constraint 3;
  A = vcat(A, sd_A); // Constraint 4;
  A = vcat(A, su_A); // Constraint 5; 
  VectorXd b(x_b.rows() + u_b.rows() + g_b.rows() + sd_b.rows() + su_b.rows());
  b << x_b, u_b, g_b, sd_b, su_b; // Constraint 1,2,3,4,5

  //cout << "constraints complete: " << timer.elapsed() << endl;

  /* Build objective matrices */
  VectorXd diag(ALL_VARS);
  VectorXd Qx_obj  = 0.0     * VectorXd::Ones(NX*(T+1)); // x
  VectorXd Qu_obj  = 0.01    * VectorXd::Ones(NU*T);     // u
  VectorXd Qr_obj  = 0.0     * VectorXd::Ones(NU*T);     // r
  VectorXd Qq_obj  = 0.0     * VectorXd::Ones(NU*NX*T);  // q
  VectorXd Qg_obj  = 10000.0    * VectorXd::Ones(NX);       // goal_diff
  VectorXd Qsg_obj = 10.0/NS * VectorXd::Ones(NX*NS);    // sample_diff
  VectorXd Qsu_obj = 0.01/NS * VectorXd::Ones(NU*NS*T);  // sample_control
  diag << Qx_obj, Qu_obj, Qr_obj, Qq_obj, Qg_obj, Qsg_obj, Qsu_obj; 
  SparseMatrix<double> Q_obj = spdiag(diag);
  SparseVector<double> c_obj = toSparseVector(VectorXd::Zero(ALL_VARS)); 

  double lb[ALL_VARS], ub[ALL_VARS];

  // default bounds 
  for (int i = 0; i < ALL_VARS; i++) { 
    lb[i] = -1e30; // treated as - infinity
    ub[i] =  1e30; // treated as + infinity 
  }

  // trust constraints
  int ind = 0; 
  for (int i = ind; i < NX*(T+1); i++) {
    lb[i] = -rho_x;
    ub[i] = rho_x; 
  }

  ind += NX*(T+1); 
  for (int i = ind; i < ind + NU*T; i++) { 
    lb[i] = u_bar(i-ind) - rho_u;
    ub[i] = u_bar(i-ind) + rho_u; 
  }

  char constraint_sense[b.rows()];
  for (int i = 0; i < b.rows(); i++) {
    constraint_sense[i] = GRB_EQUAL; 
  } 

  VectorXd opt_sol; 
  double obj;

  //cout << "start on dense optimize: " << endl;
  timer.restart(); 
  GRBEnv* env = new GRBEnv(); 
  dense_optimize(env,ALL_VARS,c_obj,Q_obj,A,constraint_sense,b,lb,ub,NULL,opt_sol,&obj);
  delete env; 

  //cout << "dense optimize time: " << timer.elapsed() << endl; 
  //parse solution
  ind = 0; 
  VectorXd opt_x  = opt_sol.segment(ind, NX*(T+1)); ind += NX*(T+1);
  VectorXd opt_u  = opt_sol.segment(ind, NU*T); ind += NU*T;
  VectorXd opt_r  = opt_sol.segment(ind, NU*T); ind += NU*T;
  VectorXd opt_q  = opt_sol.segment(ind, NU*NX*T); ind += NU*NX*T; 
  VectorXd opt_d  = opt_sol.segment(ind, NX); ind += NX;
  VectorXd opt_sd = opt_sol.segment(ind, NX*NS); ind += NX*NS;
  VectorXd opt_su = opt_sol.segment(ind, NU*NS*T); ind += NU*NS*T;

  // parse X
  opt_x = opt_x + x_bar; 
  opt_X.resize(T+1);
  for (int i = 0; i < T+1; i++) {
    opt_X[i] = opt_x.segment(i*NX, NX); 
  }

  // parse U
  opt_U.resize(T);
  for (int i = 0; i < T; i++) { 
    opt_U[i] = opt_u.segment(i*NU, NU); 
  }

  // parse Q
  SparseMatrix<double> opt_Q;
  //vector<SparseMatrix<double> > Q_diag(T); 
  for (int i = 0; i < T; i++) {
    MatrixXd Q_diag_t(NU, NX);
    VectorXd Q_diag_t_vec = opt_q.segment(NX*NU*i, NX*NU);
    for (int r = 0; r < NU; r++) {
      Q_diag_t.row(r) = Q_diag_t_vec.segment(r*NX, NX).transpose(); 
    }
    //Q_diag[i] = toSparse(Q_diag_t);
    opt_Q = blkdiag(opt_Q, toSparse(Q_diag_t)); 
  }
  opt_Q = hcat(opt_Q, spzeros(NU*T, NX)); 
  //cout << opt_Q << endl;
  //cout << opt_r << endl; 
  
  
  if (compute_policy) { 
    SparseMatrix<double> F_inv = speye(NU*T) + opt_Q*H;
    // Solve the system F_inv * K = Q 
    //MatrixXd K;
    env = new GRBEnv();
    //cout << "solving large system" << endl;
    solve_matrix_linear_system(env, F_inv, opt_Q, opt_K);
    delete env;

    cout << "solving small sys" << endl;
    //VectorXd u0; 
    env = new GRBEnv(); 
    //solve_linear_system(env, F_inv, opt_r, opt_u0);
    delete env;
  } else {
    opt_K = opt_Q;
    opt_u0 = opt_r;
  }

  //PastixLU<SparseMatrix<double> > solver;
  //solver.compute(F_inv);
  //K = solver.solve(toDense(opt_Q)); 

  //cout << F_inv*K << endl;
  //ConjugateGradient<SparseMatrix<double> > solver;
  //BiCGSTAB<SparseMatrix<double> > solver(F_inv); 
  //SparseMatrix<double> K(NU*T, NX*(T+1));
  //solver.compute(F_inv); 
  //K = solver.solve(opt_Q); 
  // Solve the system F_inv * u0 = r
  //ConjugateGradient<SparseMatrix<double> > solver;
  //SparseMatrix<double> u0(NU*T, 1);
  //u0 = solver.solve(toSparse(opt_r));


  /*
  cout << "opt_x" << endl; 
  cout << opt_x << endl;
  cout << "x_bar" << endl;
  cout << x_bar << endl;
  cout << "opt_Q*x0 + opt_r" << endl;
  cout << opt_Q*x0 + opt_r << endl; 
  cout << "K*opt_x + u0" << endl;
  cout << K*opt_x + u0 << endl; 
  cout << "opt_u" << endl; 
  cout << opt_u << endl;
  cout << "u_bar" << endl;
  cout << u_bar << endl; 

  */

  /*
  cout << "X" << endl;
  cout << opt_x << endl;

  cout << "X_bar" << endl; 
  cout << x_bar << endl; 

  cout << "U" << endl; 
  cout << opt_u << endl;

  cout << "U_bar" << endl;
  cout << u_bar << endl; 

  cout << "diff X" << endl; 
  cout << opt_x - x_bar << endl; 

  cout << "diff U" << endl;
  cout << opt_u - u_bar << endl; 
  */

}

