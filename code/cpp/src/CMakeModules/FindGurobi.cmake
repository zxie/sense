FIND_PATH(GUROBI_INCLUDE_DIR
          NAMES "gurobi_c++.h" "gurobi_c.h"
          PATHS /Library/gurobi501/mac64/include /usr/local/gurobi501/linux64/include/
          DOC "Gurobi include directory")

FIND_LIBRARY(GUROBI_CPP_LIB
             NAMES gurobi_c++ 
             PATHS /Library/gurobi501/mac64/lib /usr/local/gurobi501/linux64/lib/
             DOC "Gurobi Libraries")

FIND_LIBRARY(GUROBI_LIB
             NAMES gurobi50
             PATHS /Library/gurobi501/mac64/lib /usr/local/gurobi501/linux64/lib/
             DOC "Gurobi Libraries")

set(GUROBI_LIBRARIES ${GUROBI_CPP_LIB} ${GUROBI_LIB})

