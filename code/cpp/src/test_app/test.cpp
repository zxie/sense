#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "robots.h"
#include "car.h"

using namespace Eigen;
using namespace std;


int main()
{
  MatrixXd m(2,2);
  m(0,0) = 3;
  m(1,0) = 2.5;
  m(0,1) = -1;
  m(1,1) = m(1,0) + m(0,1);
  std::cout << "Here is the matrix m:\n" << m << std::endl;
  VectorXd v(2);
  v(0) = 4;
  v(1) = v(0) - 1;
  std::cout << "Here is the vector v:\n" << v << std::endl;

  int size = 3;
  SparseMatrix<double> mat(size, size);
  for (int i = 0; i < size; i++) { 
    mat.coeffRef(i,i) = 1.0;
  }
  SparseMatrix<double> matmat(size, 2*size);
  matmat.middleCols(0,size) = mat;
  mat.coeffRef(0,0)= 2.0;
  matmat.middleCols(size,size) = mat;


  //cout << mat << endl;
  //cout << matmat << endl;
  
}
