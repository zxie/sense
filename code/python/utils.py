import numpy as np
import random

'''
Useful functions on top of numpy
'''

def mat2tuple(npmat):
    nparr = np.array(npmat)
    return tuple(tuple(x) for x in nparr)

def scalar(m):
    if np.isscalar(m):
        return m
    if len(m.shape) < 2:
        return m[0]
    return m.item((0, 0))
    
def to_vec(arr):
    return np.array(arr).flatten()
    
def mag(arr):
    vec = to_vec(arr)
    return np.sqrt(np.vdot(vec, vec))

def shape2(mat):
    # Return (1, 1) in scalar case
    if np.shape(mat):
        return np.shape(mat)
    return (1, 1)

# More like MATLAB behavior for matrix multiplication

def zeros2(tup):
    return np.matrix(np.zeros(tup))

def ones2(tup):
    return np.matrix(np.ones(tup))

def eye2(tup):
    return np.matrix(np.eye(tup))

def rand():
    return random.random()
    
# matplotlib

COLORS = 'bgycmrk' #TODO Use color schemes in matplotlib
