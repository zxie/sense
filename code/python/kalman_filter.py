from numpy import *
from utils import *

def kf_update(A0, B0, C1, d1, Q0, R1, mu00, Sigma00, u0, z1):
    # Follows convention:
    # x1 = A0*x0 + B0*u0 + eta,   eta ~ N(0, Q0)
    # z1 = C1*x1 + d1 + delta,  delta ~ N(0, R1)

    # Dynamics update
    mu10 = A0*mu00 + B0*u0
    Sigma10 = A0*Sigma00*A0.T + Q0

    # Measurement update
    K = Sigma10*C1.T*(C1*Sigma10*C1.T + R1).I
    if z1 != None:
        mu11 = mu10 + K*(z1 - (C1*mu10 + d1))
    else:
        mu11 = mu10
    Sigma11 = (eye(A0.shape[0]) - K*C1)*Sigma10

    return (mu11, Sigma11)

def linearize_taylor(f, mu, eps=1e-3):
    # First-order Taylor series expansion of f about mu, F Jacobian
    # Numerical differentiation
    n = shape2(mu)[0]
    f_mu = f(mu)
    m = shape2(f_mu)[0]
    F = zeros2((m, n))
    for k in range(0, n):
        e_k = zeros((n, 1))
        e_k[k] = 1
        col = (f(mu + e_k*eps) - f(mu - e_k*eps))/(2*eps)
        F[:,k] = col
    return (f_mu, F)

def linearize_ols(f, mu, Sigma):
    raise Exception('linearize_ols unimplemented')

def linearize(f, mu, Sigma, method='taylor', eps=1e-3):
    if method == 'taylor':
        return linearize_taylor(f, mu, eps)
    elif method == 'ols':
        return linearize_ols(f, mu, Sigma)
    else:
        raise Exception('Invalid linearization method: ' + str(method))

def ekf_update(f0, h1, Q0, R1, mu00, Sigma00, u0, z1, method='taylor', eps=1e-3):
    # Follows convention:
    # x1 = f0(x0, u0) + eta, eta ~ N(0, Q0)
    # z1 = h1(x1) + delta, delta ~ N(0, R1)
    # f0(x0, u0) \approx a0 + F0*(x0 - mu00)
    # h1(x1) \approx c1 + H1*(x1 - mu10)
    # where a0, F0, c1, and H1 obtained by linearizing f and h respectively

    # For linearization about x, remove controls argument
    f = lambda(x): f0(x, u0)

    # Dynamics update
    (a0, F0) = linearize(f, mu00, Sigma00, method, eps)
    mu10 = a0
    Sigma10 = F0*Sigma00*F0.T + Q0

    # Measurement update
    (c1, H1) = linearize(h1, mu10, Sigma10, method, eps)
    K = Sigma10*H1.T*(H1*Sigma10*H1.T + R1).I
    if z1 != None:
        mu11 = mu10 + K*(z1 - c1)
    else: # Don't get measurement update
        mu11 = mu10
    Sigma11 = (eye2(shape(Q0)[0]) - K*H1)*Sigma10

    return (mu11, Sigma11)

def ukf_update():
    pass

def kf_smooth():
    pass

''' Some utility functions '''

def observation_update(obj, sensor):
    #TODO For now assuming static objects w/ no control inputs
    f = lambda(x, u): x
    h = sensor.observe
    Q = zeros((obj.NX, obj.NX))
    R = sensor.R
    u = 0
    z = sensor.observe(obj)
    return ekf_update(f, h, Q, R, obj.x, obj.Sigma, u, z)

