from numpy import *
from math import acos, atan2
from numpy.linalg import norm, eig, cholesky

'''
See also http://www.lfd.uci.edu/~gohlke/code/transformations.py.html
'''

#TODO def rpy2R(rpy):

#TODO def R2rpy(R):
    
#TODO def rodrigues():
    # Super elegant according to Prof. Malik, lol

def unscented_transform(mu_x, Sigma_x, f):
    L = Sigma_x.shape[0]
    h = 3.0 - L
    square_root_matrix = cholesky(mat((L + h) * Sigma_x))
    pos_samples = []
    neg_samples = []
    weights = []
    for i in range(L):
        pos_samples.append(mu_x + square_root_matrix[:,i])
        neg_samples.append(mu_x - square_root_matrix[:,i])
        weights.append( 1.0 / (2.0 * (L + h)) )

    pos_transform = []
    neg_transform = []
    for i in range(L):
        pos_transform.append(f(pos_samples[i]))
        neg_transform.append(f(neg_samples[i]))

    mu_y = float(h) / (L + h) * f(mu_x)
    for i in range(L):
        mu_y = mu_y + weights[i] * pos_transform[i]
        mu_y = mu_y + weights[i] * neg_transform[i]

    Sigma_y = 2.0 * (f(mu_x) - mu_y) * ((f(mu_x) - mu_y).T)

    for i in range(L):
        diff_pos = (pos_transform[i] - mu_y)
        diff_neg = (neg_transform[i] - mu_y)
        Sigma_y = Sigma_y + weights[i] * diff_pos*diff_pos.T
        Sigma_y = Sigma_y + weights[i] * diff_neg*diff_neg.T


    return mu_y, Sigma_y


def theta2R(theta):
    return array([[cos(theta), -sin(theta)], [sin(theta), cos(theta)]])

def R2theta(R):
    theta = acos(R[0,0]);
    if abs(sin(theta) - R[1,0]) > 1e-5:
        theta = -theta
    return theta

def q2R(q): # Returns non-homogenous matrix
    q = array(q, dtype=float)
    q = q/norm(q)
    [x, y, z, w] = q
    row1 = [w**2 + x**2 - y**2 - z**2, 2*x*y - 2*w*z, 2*w*y + 2*x*z]
    row2 = [2*w*z + 2*x*y, w**2-x**2+y**2-z**2, 2*y*z - 2*w*x]
    row3 = [2*x*z - 2*w*y, 2*w*x + 2*y*z, w**2-x**2-y**2+z**2]
    return array([row1, row2, row3])

def R2q(R):
    evals, evecs = eig(R-eye(3))
    d = abs(evals)
    inds = argsort(d)
    if d[inds[0]] > 1e-3:
        raise Exception('Invalid rotation matrix')
    axis = evecs[:,inds[0]]
    axis = axis.real
    if abs(norm(axis) - 1) > 1e-4:
        raise Exception('Non-unit rotation axis')
    twocostheta = trace(R)-1
    twosinthetav = matrix([R[2,1]-R[1,2], R[0,2]-R[2,0], R[1,0]-R[0,1]])
    twosintheta = axis*twosinthetav.T
    theta = atan2(twosintheta, twocostheta)
    return hstack([axis*sin(theta/2), array([cos(theta/2)])])



