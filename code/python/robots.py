from numpy import *
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib import rc
from objects import SimObject
from utils import scalar
from covar import draw_ellipsoid, vec2cov, cov2vec,\
    project_psd
from kalman_filter import ekf_update
from numpy.random import multivariate_normal as mvn
import time
from math import atan2, atan

'''
All of these implementations are in 2D world, for 3D use OpenRAVE...
'''

class Robot(SimObject):

    NX = -1 # Size of each state
    NU = -1 # Size of each control
    EPS =  1e-3 # Used for linearization

    robot_index = 0

    @classmethod
    def increment_index(cls):
        cls.robot_index += 1
        return cls.robot_index

    def __init__(self, x, Sigma=None, dt=0.1):
        self.NB = self.NX+self.NX**2 # Size of belief state 
        self.x = x   # State
        self.Sigma = Sigma # Covariance
        self.dt = dt # Time step
        self.sensors = [] # Array of (sensor, pos_fn) pairs

    def __str__(self):
        raise Exception('__str__ function unimplemented')

    def dynamics(self, x, u):
        # Dynamics update from state x with control u
        # Returns state x so can update state or just simulate
        raise Exception('dynamics function unimplemented')

    def cov_dynamics(self, x, u, Sigma0, sim_env, Q, R):
        #TODO Refactor sim_env, Q, R
        mu, Sigma1 = ekf_update(self.dynamics,
                               lambda x: self.observe(sim_env, x=x),
                               Q, R, x, Sigma0, u, None, eps=self.EPS)
        return Sigma1

    def belief_dynamics(self, b, u, sim_env, Q, R):
        # Belief vector b = [x; vec(Sigma)] 
        #FIXME Currently just assuming belief captured by Gaussian
        #TODO Refactor sim_env, Q, R
        x = b[0:self.NX]
        Sigma = vec2cov(b[self.NX:])
        Sigma = project_psd(Sigma)
        x1 = self.dynamics(x, u)
        Sigma1 = self.cov_dynamics(x, u, Sigma, sim_env, Q, R)
        b = vstack((x1, cov2vec(Sigma1)))
        return b

    def linearize_belief_dynamics(self, Bel_bar, u_bar, sim_env, Q, R):
        f = lambda b, u: self.belief_dynamics(b, u, sim_env, Q, R)
    
        A = mat(zeros((self.NB, self.NB)))
        for m in xrange(self.NB):
            eps_vec = mat(zeros(self.NB)).T;
            eps_vec[m] = self.EPS
            A[:,m] = (f(Bel_bar+eps_vec, u_bar) - f(Bel_bar-eps_vec, u_bar)) /\
                (2*self.EPS)

        B = mat(zeros((self.NB, self.NU)))
        for m in xrange(self.NU):
            eps_vec = mat(zeros(self.NU)).T
            eps_vec[m] = self.EPS
            B[:,m] = (f(Bel_bar, u_bar+eps_vec) - f(Bel_bar, u_bar-eps_vec)) /\
                (2*self.EPS)
       
        c = self.belief_dynamics(Bel_bar, u_bar, sim_env, Q, R)

        return A, B, c 

    def linearize_belief_dynamics_trajectory(self, Bel_bar, U_bar, sim_env, Q, R):
        T = Bel_bar.shape[1]

        As = zeros((self.NB, self.NB, T-1))
        Bs = zeros((self.NB, self.NU, T-1))
        Cs = mat(zeros((self.NB, T-1)))

        for t in xrange(T-1):
            A, B, c = self.linearize_belief_dynamics(Bel_bar[:,t], U_bar[:,t],\
                                                      sim_env, Q, R)
            As[:,:,t] = A
            Bs[:,:,t] = B
            Cs[:,t] = c

        return As, Bs, Cs

    def linearize_dynamics(self, x_bar, u_bar):
        # First order linearization
        # Return A, B and c matrices s.t.
        # x1 = A*(x0-x_bar) + B*(u-u_bar) + c

        f = self.dynamics
        A = mat(zeros((self.NX, self.NX)))
        for m in xrange(self.NX):
            eps_vec = mat(zeros(self.NX)).T;
            eps_vec[m] = self.EPS
            A[:,m] = (f(x_bar+eps_vec, u_bar) - f(x_bar-eps_vec, u_bar)) /\
                (2*self.EPS)

        B = mat(zeros((self.NX, self.NU)))
        for m in xrange(self.NU):
            eps_vec = mat(zeros(self.NU)).T
            eps_vec[m] = self.EPS
            B[:,m] = (f(x_bar,u_bar+eps_vec) - f(x_bar, u_bar-eps_vec)) /\
                (2*self.EPS)
       
        c = self.dynamics(x_bar, u_bar)

        return A, B, c 

    def linearize_dynamics_trajectory(self, X_bar, U_bar):
        T = X_bar.shape[1]

        As = zeros((self.NX, self.NX, T-1))
        Bs = zeros((self.NX, self.NU, T-1))
        Cs = mat(zeros((self.NX, T-1)))

        for t in xrange(T-1):
            A, B, c = self.linearize_dynamics(X_bar[:,t], U_bar[:,t])
            As[:,:,t] = A
            Bs[:,:,t] = B
            Cs[:,t] = c

        return As, Bs, Cs

    #FIXME Write function which generates samples separately? Might be
    # case where want to use same set of samples

    def collision_penalty_trajectory(self, Bel_bar_vec, sim_env,\
            num_samples_per_state=1000, plot_samples=False):
        # Bel_bar vec is stacked vector of belief states
        #TODO Could make cost smoother by adding term of how far into the obstacle
        # the sampled points are
        Bel_bar = Bel_bar_vec.reshape((self.NB,-1), order='F')
        c = 0
        for k in xrange(Bel_bar.shape[1]):
            x = array(Bel_bar[0:self.NX, k]).flatten()
            Sigma = vec2cov(Bel_bar[self.NX:,k])
            samples = mvn(x, Sigma, num_samples_per_state).T
            if plot_samples:
                ax = plt.gca()
                ax.plot(samples[0,:], samples[1,:], 'o')
            collides = sim_env.in_collision(samples[0:2,:])
            collisions = sum(collides) # True -> 1, False -> 0
            #if collisions > 0 #TODO Smooth cost
            c = c + collisions/float(num_samples_per_state)
        if plot_samples:
            plt.show()
            stop
        return c 

    def collision_penalty_jacobian_trajectory(self, Bel_bar, U_bar, sim_env, eps=0.01):
        #NOTE Large linearization, may not work for small obstacles
        #NOTE U_bar unused, also Jacobian w.r.t. collision for controls somehow?
        f = lambda x: self.collision_penalty_trajectory(x, sim_env)
        J_A = mat(zeros((Bel_bar.size, 1)))
        for k in xrange(Bel_bar.shape[1]):
            bel = Bel_bar[:,k]
            for m in xrange(Bel_bar.shape[0]):
                eps_vec = zeros(bel.shape)
                eps_vec[m] = eps
                J_A[bel.size*k+m] = (f(bel+eps_vec) - f(bel-eps_vec))/(2*eps)
        return J_A

    def attach_sensor(self, sensor, pos_fn):
        # Add a sensor to list, pos_fn takes robot state of returns/sets
        # world coordinates of sensor
        self.sensors.append((sensor, pos_fn))

    def observe(self, scene, x=None):
        if x==None:
            x = self.x
        zs = array([])
        for (sensor, pos_fn) in self.sensors:
            #sensor.move_to(*pos_fn(x))
            z = sensor.observe(scene, x=pos_fn(x))
            if zs.shape[0] == 0:
                zs = z
            else:
                zs = vstack((zs, z))
        return zs

    def draw(self, X=None):
        # Visualize robot
        raise Exception('draw function unimplemented')

    def draw_trail(self, xs, color='b'):
        if hasattr(self, 'traj_pos'):
            pts = [self.traj_pos(x) for x in xs]
            xs = [pt[0] for pt in pts]
            ys = [pt[1] for pt in pts]
            plt.plot(xs, ys, color=color)
            '''
            rc('text', usetex=True)
            plt.text(xs[0], ys[0], r'$x_0$', size=12, color='purple')
            plt.text(xs[-1], ys[-1], r'$x_T$', size=12, color='green')
            '''

    def draw_trajectory(self, xs, mus=None, Sigmas=None, color='cyan'):
        for x in xs:
            self.draw(x, color=color)
        if mus == None or Sigmas == None:
            return
        for k in xrange(mus.shape[1]):
            draw_ellipsoid(mus[:,k], Sigmas[:,:,k], color=color)

    def draw_belief_trajectory(self, bels, color='cyan'):
        for k in xrange(bels.shape[1]):
            bel = bels[:,k]
            x = bel[0:self.NX]
            Sigma = project_psd(vec2cov(bel[self.NX:]))
            self.draw(x.flat)
            draw_ellipsoid(x[0:2], Sigma[0:2,0:2], color=color)

    def get_animation_artists(self, bels, color='cyan'):
        #TODO Abstract away this artist scheme, create animate_tools.py or
        # something
        ims = list()
        for k in xrange(bels.shape[1]):
            bel = bels[:,k]
            x = bel[0:self.NX]
            Sigma = project_psd(vec2cov(bel[self.NX:]))
            body = self.draw(x.flat)
            ell = draw_ellipsoid(x[0:2], Sigma[0:2,0:2], color=color)
            ims.append([body, ell])
        return ims

class LocalizerBot(Robot):

    NX = -1
    NU = -1

    def __init__(self, bot, obj):
        self.bot = bot
        self.NX = bot.NX + 2 #FIXME (hack for now)
        self.NU = bot.NU
        self.dt = bot.dt
        
        x = array(zeros((self.NX)))
        for t in range(bot.NX):
            x[t] = bot.x[t]
        x[bot.NX] = obj[0]
        x[bot.NX+1] = obj[1]
        #x = array([bot.x, obj]).ravel()
        #x.shape = (self.NX, 1)
        Robot.__init__(self, x, dt=self.dt)
  
    def dynamics(self, X, u):
        bot_up = self.bot.dynamics(X[0:self.bot.NX], u)
        return vstack((bot_up, X[self.bot.NX:]))
  
    def fov_state(self, x):
        xy = mat(self.bot.traj_pos(x)).T
        theta = self.bot.orientation(x)
        #print vstack((xy, theta, x[self.bot.NX:]))
        if isinstance(x, tuple) or len(x.shape) == 1:
            x = mat(x).T
        if isinstance(xy, tuple) or xy.shape[0] < xy.shape[1]:
            xy = mat(xy).T
        return vstack((xy, theta, x[self.bot.NX:]))
  
    def observe(self, scene, x=None):
        zs = self.bot.observe(scene, x)
        return vstack((zs, Robot.observe(self, scene, x)))
  
    def draw(self, x=None, color='blue'):
        self.bot.draw(x, color)

        if x == None:
            x = self.x

        #FIXME HACK
        #plt.plot([self.x[self.NX-2]], [self.x[self.NX-1]], 'bo')

        #for (sensor, pos_fn) in self.sensors:
        #    sensor.move_to(*pos_fn(x[0:3]))
        #    sensor.draw()
  
    def mark_fov(self, xbot, scene, bounds, color='y'):
        delta = 0.01
        xs = arange(xbot[0]-0.2, xbot[0]+0.2, delta)
        ys = arange(xbot[1]-0.2, xbot[1]+0.2, delta)
        print len(xs) * len(ys)
        ax = plt.gca()
        for j in xrange(len(xs)):
            for k in xrange(len(ys)):
                x = xs[j]; y = ys[k];
                merge_state = vstack((xbot[0:self.bot.NX], x, y))
                if (abs(Robot.observe(self, scene, merge_state))) < 250:
                    ax.plot(x, y, 'o', color=color, markersize=2)
            
          
