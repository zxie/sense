import random
import unittest
import sys; sys.path.append('..')
from numpy import *
from sensors import PinholeCamera2D
from utils import *

class TestSensors(unittest.TestCase):

    def setUp(self):
        self.pinhole_camera_2d = PinholeCamera2D(500, np.array([0,0,0]), 500)

    def test_pinhole_camera_2d(self):
        u = self.pinhole_camera_2d.project_point(array([[1], [0]]))
        self.assertEqual(u, 0)
        u = self.pinhole_camera_2d.project_point(array([[1], [1]]))
        self.assertTrue(u == self.pinhole_camera_2d.f)
        u = self.pinhole_camera_2d.project_point(array([[1], [-1]]))
        self.assertTrue(u == -self.pinhole_camera_2d.f)

    def test_pinhole_camera_3d(self):
        pass

if __name__ == '__main__':
    unittest.main()

