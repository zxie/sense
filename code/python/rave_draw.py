from numpy import * 
from scipy.stats import chi2
import os

SPHERE_DATA_FILE = 'sphere_points.txt'

def load_sphere():
  path = os.path.join(os.path.dirname(__file__), SPHERE_DATA_FILE) 
  dat = fromfile(path, sep=' ')
  dat.shape = (len(dat)/3, 3)
  return mat(dat)


def draw_ellipsoid(mu, Sigma, std_dev, env, colors=array((1.0,0.0,0.0,0.2))):
  # http://jellymatter.com/2011/03/31/drawing-confidence-ellipses-and-ellipsoids/

  sphere_points = load_sphere()

  k = std_dev

  # Compute the axis and scale of the ellipsoid described by the covariance matrix
  rotation_scaling_transformation = linalg.cholesky(Sigma)

  # Transform the sphere into the desired ellipsoid
  ellipsoid_points = mu + k * rotation_scaling_transformation * sphere_points.T
  ellipsoid_points = ellipsoid_points.T

  # Plot the data
  plot_points = array(ellipsoid_points).ravel()
  plot_points.shape = (len(plot_points)/3,3)
  ellipsoid = env.drawtrimesh(points=plot_points, indices=None, colors=colors)

  return ellipsoid 

def draw_sphere(mu, r, env, colors=array((1.0,0.0,0.0,0.2))):
  cov = mat(r*identity(3))
  return draw_ellipsoid(mu=mu, Sigma=cov, std_dev=1, env=env, colors=colors)


def draw_beacon(mu, env):
  sphere1 = draw_sphere(mu, 0.25, env, colors=array((1.0,1.0,1.0,0.5))) 
  sphere2 = draw_sphere(mu, 0.5, env, colors=array((1.0,1.0,1.0,0.25))) 
  sphere3 = draw_sphere(mu, 1.0, env, colors=array((1.0,1.0,1.0,0.10))) 
  sphere4 = draw_sphere(mu, 2.0, env, colors=array((1.0,1.0,1.0,0.05))) 

  return [sphere1, sphere2, sphere3, sphere4]
