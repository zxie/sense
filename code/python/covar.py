from numpy import *
from scipy.linalg import eig
from scipy.stats import chi2
from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt
from utils import *
from math import atan2

# Utilities related to covariance matrices

def psd(A):
    eigvals, eigvecs = eig(A)
    return allclose((eigvals-eigvals), zeros(eigvals.shape))

def symmetric(A):
    return allclose(A, A.T)

def valid_cov(Sigma):
    return symmetric(Sigma) and psd(Sigma)

def cov2vec(Sigma):
    # Symmetric so either row/column-major order is fine
    return np.mat(Sigma.ravel()).T

def vec2cov(v):
    n = int(sqrt(max(v.shape)))
    return mat(v.reshape((n,n)))

def project_psd(Sigma):
    # If covariance is negative definite take EVD to project to set
    # of PSD matrices
    eigvals, eigvecs = eig(Sigma)
    for k in xrange(len(eigvals)):
        eigvals[k] = max(eigvals[k], 0)
    return mat(eigvecs)*mat(diag(eigvals))*mat(eigvecs.T)
    
def draw_ellipsoid(mu, Sigma, conf=0.95, color='yellow'):
    assert valid_cov(Sigma)
    n = shape(Sigma)[0]
    semiaxes = conf_ellipsoid_axes(Sigma, conf)
    angle = atan2(semiaxes[1,0], semiaxes[0,0])
    if n==2:
        ell = Ellipse(mu, mag(semiaxes[:,0])*2, mag(semiaxes[:,1])*2,
                      rad2deg(angle), edgecolor=color, facecolor='none')
        ax = plt.gca()
        ax.add_patch(ell)
        return ell
    else:
        raise Exception('draw_ellipsoid only works for 2D')

def conf_ellipsoid_axes(Sigma, conf=0.95):
    # Return scaled semiaxes of confidence ellipsoid
    # parameterized by covariance matrix and confidence
    n = shape(Sigma)[0]
    if not 0 < conf < 1:
        raise Exception('Invalid confidence: ' + str(conf))
    # (x-x_bar)^T*inv(Sigma)*(x-x_bar) has Chi^2 distribution
    Sigma = Sigma*chi2.ppf(conf, n)
    eigvals, eigvecs = eig(Sigma)
    assert all(eigvals >= 0), 'Sigma not PSD: ' + str(Sigma)
    semiaxes = eigvecs
    for k in xrange(n):
        semiaxes[:,k] *= sqrt(eigvals[k])
    return semiaxes      


