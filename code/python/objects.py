from numpy import array
import numpy as np
import matplotlib.pyplot as plt
from kalman_filter import ekf_update
from covar import draw_ellipsoid

class SimObject:

    NX = 1
    object_index = 0

    def __init__(self, x, Sigma=None):
        # Convert to n-by-1 mat
        #TODO
        # Within FOVSensor and probably other places there is the assumption
        # that x[0:2] contains the x and y position of the object in 2D
        self.x = x # State
        self.Sigma = Sigma # Covariance ellipsoid

    @classmethod
    def increment_index(cls):
        cls.object_index += 1
        return cls.object_index

    def linearize_dynamics(self):
        # Assuming object is static
        # Doesn't respond to controls
        # At some point may want dynamic objects

        A = np.mat(np.zeros((self.NX, self.NX)))
        B = np.mat(np.zeros((self.NX, self.NU)))
        c = self.x

        return A, B, c 

    def linearize_dynamics_trajectory(self, T):

        As = zeros((self.NX, self.NX, T-1))
        Bs = zeros((self.NX, self.NU, T-1))
        Cs = np.tile(self.x, (1, T-1))

        return As, Bs, Cs

class Point2D(SimObject):
    # Static point w/ some Gaussian uncertainty

    NX = 2
    
    def __init__(self, x, Sigma=np.array([[0, 0], [0, 0]]),\
            color='purple'):
        SimObject.__init__(self, x, Sigma=Sigma)
        self.index = Point2D.increment_index()
        self.color = color

    def draw(self):
        ax = plt.gca()
        ax.plot(self.x[0], self.x[1], 'o', color=self.color)
        #ax.text(self.x[0], self.x[1], str(self.index))
        if abs(np.sum(self.Sigma)) > 0:
            draw_ellipsoid(self.x, self.Sigma, color=self.color)

'''
class LineSegment2D(SimObject):

    def __init__(self, pt1, pt2):
        self.end_pt1 = pt1
        self.end_pt2 = pt2
        self.index = LineSegment2D.increment_index()

    def __init__(self, x1, x2):
        self.end_pt1 = Point2D(x1)
        self.end_pt2 = Point2D(x2)

    def vec(self):
        return array((self.endpt1.vec(), self.endpt2.vec()))
'''

""" Below likely won't be used """

class Point3D(SimObject):
    
    def __init__(self, x):
        self.x = x # x, y, z
        self.index = Point3D.increment_index()

    def draw(self, ax3d):
        ax3d.scatter([self.x[0]], [self.x[1]], zs=[self.x[2]], marker='o')
        ax3d.text(self.x[0], self.x[1], self.x[2], str(self.index))


