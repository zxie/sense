function [K, P] = lqr_time_varying_controller(A, B, Q, R, Qfinal)

% Now let's use the Value Iteration Solution to LQR control problems
% to solve the following optimal control problem:
% min_{x,u} \sum_{t=1}^T x(:,t)'*Q*x(:,t) + u(:,t)'*R*u(:,t) + x(:,T+1)'*Qfinal*x(:,T+1)
% s.t.      x(:,t+1) = A{t}*x(:,t) + B{t}*u(:,t)

% Problem has been defined, let's solve it:

% LQR Value Iteration back-ups to find:
% K{k}, k=1,2,...,T : feedback control for k time steps to-go if in state x equals K{k}*x
% P{k}, k=1,2,...,T : cost-to-go for k time steps to-go if in state x equals x'*P{k}*x

T = size(A, 2);

P_prev = Qfinal;

K = cell(T, 1); P = cell(T, 1);
for k=1:T
    K{k} = -inv(R + B{T-k+1}'*P_prev*B{T-k+1})*B{T-k+1}'*P_prev*A{T-k+1};
    if k==1
        Qk = Qfinal;
    else
        Qk = Q;
    end
    P{k} = Qk + K{k}'*R*K{k} + (A{T-k+1} + B{T-k+1}*K{k})'*P_prev*(A{T-k+1} + B{T-k+1}*K{k});
    P_prev = P{k};
end
