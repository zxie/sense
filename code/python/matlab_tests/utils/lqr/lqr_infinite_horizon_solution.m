function [K, P] = lqr_infinite_horizon_solution(A, B, Q, R)

%% find the infinite horizon K and P through running LQR back-ups
%%   until norm(K_new - K_current, 2) <= 1e-4  

P = 0;

while 1
    K = -inv(R+B'*P*B)*B'*P*A;
    P = Q + K'*R*K + (A + B*K)'*P*(A + B*K);
    if exist('K_prev', 'var') && norm(K - K_prev, 2) <= 1e-4
        break
    end
    K_prev = K;
end