function [H] = num_hess(func, X, eps)

H = zeros(length(X));

% for each dimension of objective function
for i=1:length(X)
    % derivative at first point (left)
    x1 = X;
    x1(i) = X(i) - eps;
    df1 = num_grad(func, x1, eps);
    
    % derivative at second point (right)
    x2 = X;
    x2(i) = X(i) + eps;
    df2 = num_grad(func, x2, eps);
    
    % differentiate between the two derivatives
    d2f = (df2-df1) / (2*eps);
    
    % assign as row i of Hessian
    H(i,:) = d2f';
end