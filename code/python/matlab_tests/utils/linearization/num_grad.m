function [df] = num_grad(func, X, eps)

df = zeros(length(X), 1);

% for each dimension of objective function
for i=1:length(X)
    % vary variable i by a small amount (left and right)
    x1 = X;
    x2 = X;
    x1(i) = X(i) - eps;
    x2(i) = X(i) + eps;
    
    % evaluate the objective function at the left and right points
    y1 = func(x1);
    y2 = func(x2);
    
    % calculate the slope (rise/run) for dimension i
    df(i) = (y2 - y1) / (2*eps);
end