function [ J_H ] = estimate_ball_observation_jacobian(X_car)
%computes
%J_H = \partial h(X)  / \partial X

    global balls;
    vec_balls = balls2vec(balls); 
    X = vec_balls; 
    
    
    eps = 1e-2;
        
    J_H = zeros(length(balls), length(X)); 
    for i = 1:length(X)
        dx = zeros(length(X), 1);
        dx(i) = eps; 
        
        h_xu = ball_observation(X_car, X + dx, zeros(length(balls), 1));
        J_H(:,i) = h_xu; 
        
        dx(i) = -eps;
        
        h_xu = ball_observation(X_car, X + dx, zeros(length(balls), 1));
        J_H(:,i) = J_H(:,i) - h_xu; 
    end
    J_H = J_H ./ (2*eps); 
    
 

end


