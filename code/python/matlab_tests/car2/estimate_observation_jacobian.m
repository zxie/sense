function [ J_H ] = estimate_observation_jacobian(X)
%computes
%J_H = \partial h(X)  / \partial X

    global observation_length; 
    eps = 1e-2;
        
    J_H = zeros(observation_length, length(X)); 
    for i = 1:length(X)
        dx = zeros(length(X), 1);
        dx(i) = eps; 
        
        h_xu = observation(X + dx, zeros(observation_length, 1));
        J_H(:,i) = h_xu; 
        
        dx(i) = -eps;
        
        h_xu = observation(X + dx, zeros(observation_length, 1));
        J_H(:,i) = J_H(:,i) - h_xu; 
    end
    J_H = J_H ./ (2*eps); 
    
    
    %J_H = zeros(observation_length, length(X)); 

 

end


