function [  ] = plot_fov( car_state )
%plots the fov of the car state
global limits;
global fov_angle; 

X = car_state;
xc = X(1);
yc = X(2);

x_l = xc - 0.2;
x_h = xc + 0.2;
y_l = yc - 0.2;
y_h = yc + 0.2;

step = 0.02;
x = x_l:step:x_h;
y = y_l:step:y_h;

theta = X(3);
R = [cos(theta) -sin(theta); 
    sin(theta) cos(theta)];
unitX = [1; 0]; 
car_dir = R*unitX; 
car_pos = [X(1); X(2)];


for i = 1:length(x)
    for j = 1:length(y)
        ball_pos = [x(i); y(j)]; 
        del = ball_pos - car_pos ;
        Z(j,i) = 0; 
        if( (car_dir' * del)/(norm(car_dir) * norm(del)) >= cos(fov_angle) ) 
           Z(j,i) = 1 / norm(del) ;
           
           scatter(ball_pos(1), ball_pos(2),1,'r','filled');
        end
        

    end
end

%contourf(x,y,Z,100); 
%colormap gray
%shading flat 



end

