function [ output_args ] = plot_obstacles( obstacles )
%obst{1} = radius
%obst{2} = [xc yc]


for i = 1:length(obstacles)
    obst = obstacles{i};
    w = obst{1} * 2; % w is diameter
    xc = obst{2}(1);
    yc = obst{2}(2);
    gray = [0.7 0.7 0.7];
    rectangle('Curvature',[1 1], ...
              'Position',[xc-w/2 yc-w/2 w w], ...
              'FaceColor', gray);
    axis equal;

end

end

