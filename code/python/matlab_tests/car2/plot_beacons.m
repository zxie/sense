function [ output_args ] = plot_beacons( beacons, limits )
global balls;

vec_balls = balls2vec(balls); 

x_l = limits(1);
x_h = limits(2);
y_l = limits(1);
y_h = limits(2);

%[x,y] = meshgrid(x_l:0.1:x_h, y_l:0.1:y_h)

step = 0.05;
x = x_l:step:x_h;
y = y_l:step:y_h;

Z = zeros(length(y), length(x));

for i = 1:length(x)
    for j = 1:length(y)
        X = [x(i); y(j); 0; 0];
        n = zeros(length(beacons)+length(balls)+1, 1);
        obs = observation([X; vec_balls],n);
        Z(j, i) = norm(obs(1:length(beacons)));
    end
end

contourf(x,y,Z,1000); 
colormap gray
shading flat 




end

