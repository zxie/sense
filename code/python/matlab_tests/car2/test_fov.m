clear all; 

limits = [0 0.5];
X = [0.1; 0.0; 0.3; 0]; 
figure;hold on;

xlim(limits);
ylim(limits); 

fov_alph = 0.2; 

x_l = limits(1);
x_h = limits(2);
y_l = limits(1);
y_h = limits(2);

step = 0.005;
x = x_l:step:x_h;
y = y_l:step:y_h;

Z = zeros(length(y), length(x));

theta = X(3);
R = [cos(theta) -sin(theta); 
    sin(theta) cos(theta)];
unitX = [1; 0]; 
car_dir = R*unitX; 
car_pos = [X(1); X(2)];


for i = 1:length(x)
    for j = 1:length(y)
        ball_pos = [x(i); y(j)]; 
        del = ball_pos - car_pos ;
        Z(j,i) = 0; 
        if( (car_dir' * del)/(norm(car_dir) * norm(del)) >= cos(fov_alph) ) 
           Z(j,i) = 1 / norm(del) ;
           
           %plot(ball_pos(1), ball_pos(2));
        end
        

    end
end

contourf(x,y,Z,100); 
colormap gray
shading flat 
plot_car(X,1);
