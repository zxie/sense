function [ sigma ] = kalman_sigma(traj_X, traj_U, Sigma_0, Q, R)
% computes the kalman filter estimates of Sigma over states traj_X 
% using a linearized dynamics and observation model. This filter is over
% the car trajectory and balls 

global observation_length;
global dt; 
global NUM_CONTROLS;
global NUM_STATES;
global SIZE_EACH_CONTROL;
global balls;
%global SIZE_EACH_STATE;

A = cell(1,NUM_STATES);
B = cell(1,NUM_STATES); 
C = cell(1,NUM_STATES);
d = cell(1,NUM_STATES); 

vec_balls = balls2vec(balls); 

for i = 1:NUM_CONTROLS
    [A{i}, B{i}] = estimate_jacobian(traj_X(:,i), traj_U(:,i), dt);
    A{i} = blkdiag(A{i}, eye(length(vec_balls)));
    %B{i} isn't used, so screw it. 
    C{i} = estimate_observation_jacobian([traj_X(:,i); vec_balls]);
%    d{i} = (observation(traj_X(:,i),zeros(observation_length, 1)) - ...
%        C{i} * traj_X(:,i));
end

[A{NUM_STATES}, B{NUM_STATES}] = ...
    estimate_jacobian(traj_X(:,NUM_STATES), zeros(SIZE_EACH_CONTROL,1),dt);
A{NUM_STATES} = blkdiag(A{NUM_STATES}, eye(length(vec_balls)));
%B{.} isn't used
C{NUM_STATES} = estimate_observation_jacobian([traj_X(:,NUM_STATES); vec_balls]);
%d{NUM_STATES} = (observation(traj_X(:,NUM_STATES),zeros(observation_length,1)) - ...
%    C{NUM_STATES} * traj_X(:,NUM_STATES));

sigma = cell(1,NUM_STATES);
sigma{1} = Sigma_0;

for i = 2:NUM_STATES
    time_sigma = A{i}*sigma{i-1}*A{i}' + Q;
    K = time_sigma*C{i}'/(C{i}*time_sigma*C{i}' + R);
    sigma{i} = (eye(size(Sigma_0)) - K*C{i})*time_sigma; 
end



end

