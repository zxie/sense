% Draw 2D covariance ellipsoid
% Wrapper around plotcov2

function plot_ellipse( mu, Sigma, color)

global conf;
SCALE = conf2mahal(conf, 2);

plotcov2(mu, SCALE*Sigma, color, 'conf', conf, 'num-pts', 100, 'plot-axes', 0);

end

