NUM_VAR = 10000;
NUM_CONS = 10; 
% H = legal_cov(rand(NUM_VAR,NUM_VAR));
f = rand(NUM_VAR,1);
A = rand(NUM_CONS, NUM_VAR);
b = rand(NUM_CONS,1); 
H = eye(NUM_VAR);

zz = quadprog(H, f, A, b); 

0.5*zz'*H*zz + f'*zz
A*zz <= b

cvx_begin
variable x(NUM_VAR)

minimize(0.5*x'*H*x + f'*x)
subject to
A*x <= b;
cvx_end

