function [ h ] = observation( X, n )

global beacons;
global observation_length;
global balls;

observation_length = length(beacons) + length(balls) + 1; 
%observation_length = length(beacons) + 2; 

%car_state = X(1:4); 
%vec_ball = X(5:end); 

% X is the state, n is a noise vector sampled from (0,Q)

%position  - we do not get actual position estimates, but we will get the
%... steering wheel angle 


xp = X(1);
yp = X(2);

h = zeros(observation_length,1);

for i = 1:length(beacons)
    h(i) = 1 / (25*(xp - beacons{i}{1})^2 + 25*(yp - beacons{i}{2})^2 + 1); 
end
h(length(beacons) + 1 : length(beacons) + length(balls)) = ball_observation(X, zeros(length(balls), 1));  %% noise is added later 

h(end)     = X(4);

h = h + n;

end

