function [ vec ] = balls2vec( balls )

vec = zeros(length(balls)*2,1);

for i = 1:length(balls)
    vec(2*i-1:2*i) = balls{i}{1}; 
end


end

