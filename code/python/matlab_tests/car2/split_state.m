function [ X_s, X_c ] = split_state( X )
global SIZE_EACH_STATE;

X_s = X(1:SIZE_EACH_STATE);
X_c = X(SIZE_EACH_STATE+1: end);



end

