function [  ] = plot_trajectory( pstates, pcontrols, plot_type_ind )
%plots stuff


global Q;
global R;
global Sigma_0; 
global NUM_SAMPLES; 
global balls; 
global ball_Q;
global ball_R; 
global NUM_STATES;
global SIZE_EACH_STATE; 

plot_type = ['r','g','b','m','c', 'y'];
%sigmas = kalman_sigma(pstates, pcontrols, Sigma_0, Q, R); 
%ball_sigmas = kalman_sigma_balls(pstates, ball_Q, ball_R); 
for i = 1:NUM_STATES
   X = pstates(:,i);
   [X_s, X_c] = split_state(X); 
   cov = vec2cov(X_c); 
   if i ==length(pstates)
       plot_car(X_s, plot_type_ind);
   else
       plot_car(X_s, plot_type_ind);
   end
   %plot_fov(X); 
%   sig = sigmas{i};
   plot_ellipse(X(1:2), cov(1:2,1:2), plot_type(plot_type_ind));
%    mu = X;  
%    sigma = sigmas{i};
%    sample = mvnrnd(mu,sigma,NUM_SAMPLES)'; 
%    scatter(sample(1,:)', sample(2,:)', 'b'); 
   
end

%plot(pstates(1,:)', pstates(2,:)', plot_type(1,plot_type_ind));
plot_balls(balls, plot_type(1,plot_type_ind)); 
ball_1_pos = balls{1}{1};
X_N = pstates(:,NUM_STATES);
[X_s, X_c] = split_state(X_N); 
cov = vec2cov(X_c); 
scatter(ball_1_pos(1), ball_1_pos(2), plot_type(1,plot_type_ind));
plot_ellipse(balls{1}{1}, cov(5:6, 5:6), plot_type(1,plot_type_ind));


end

