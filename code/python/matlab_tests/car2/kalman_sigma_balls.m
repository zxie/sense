function [ sigma ] = kalman_sigma_balls(traj_X, Q, R)
% computes the kalman filter estimate of the balls given linearized obs. 

global NUM_STATES; 
global NUM_CONTROLS; 
global balls; 

A = cell(1,NUM_STATES);
%B = cell(1,NUM_STATES); 
C = cell(1,NUM_STATES);
%d = cell(1,NUM_STATES); 

vec_balls = balls2vec(balls); 


for i = 1:NUM_STATES
    A{i} = eye(length(vec_balls));
    C{i} = estimate_ball_observation_jacobian(traj_X(:,i));
%    d{i} = (observation(traj_X(:,i),zeros(observation_length, 1)) - ...
%        C{i} * traj_X(:,i));
end



Sigma_0 = zeros(0);
for j = 1 : length(balls)
    Sigma_0 = blkdiag(Sigma_0, balls{j}{2});
end

sigma = cell(1,NUM_STATES);
sigma{1} = Sigma_0;


for i = 2:NUM_STATES
    time_sigma = A{i}*sigma{i-1}*A{i}' + Q;
    K = time_sigma*C{i}'/(C{i}*time_sigma*C{i}' + R);
    sigma{i} = (eye(size(Sigma_0)) - K*C{i})*time_sigma; 
end



end

