function [ h ] = ball_observation(in_state, n)
%Given a ball and car_state and noise, returns the observation of the ball

global fov_angle; 
global balls;
global SIZE_EACH_STATE; 


num_balls = length(balls);
SIZE_BALL_STATE = num_balls * 2; 

h = zeros(num_balls, 1); 

X = in_state(1:SIZE_EACH_STATE - SIZE_BALL_STATE);
vec_ball = in_state(SIZE_EACH_STATE - SIZE_BALL_STATE + 1 : SIZE_EACH_STATE); 

theta = X(3);
R = [cos(theta) -sin(theta); 
    sin(theta) cos(theta)];
unitX = [1; 0]; 
car_dir = R*unitX; 
car_pos = [X(1); X(2)];


for i = 1:num_balls
    % check fov
    ball_pos = vec_ball(2*i - 1: 2*i); 
    del = ball_pos - car_pos ;
    cos_ang = (car_dir' * del)/(norm(car_dir) * norm(del));
    
%    h(i) = (1+cos_ang) / (25 * norm(del) + 1);     
    if( cos_ang >= cos(fov_angle) ) 
        % in fov, give decaying signal 
        signal = cos_ang - cos(fov_angle); 
        h(i) = signal / (15*norm(del) + 1) ;
    end
end

h = h + n; 
% h = n; 

end

