function [ Y ] = apply_control(X, U, dt)
% Applies control U to state X for time dt. Returns the state in Y. 

global balls; 
 
SIZE_BALL_STATE = length(balls)*2; 

L = 0.01; %% set the length of the car
% 
x = X(1);
y = X(2);
theta = X(3); 
v = X(4); 

u_a = U(1);
u_phi = U(2);


x_dot = v * cos(theta);
y_dot = v * sin(theta);
theta_dot = v * tan(u_phi) / L;
v_dot = u_a; 

X_dot = [x_dot; y_dot; theta_dot; v_dot; zeros(SIZE_BALL_STATE,1)]; 

Y = X + dt * X_dot; 

end

