function [ Innov_t1 ] = innov_dynamics( X_t, U_t, Sigma_t )
global dt; 
global Q;
global R; 

%check if Sigma_t is PSD, if not use EVD to turn it into PSD
Sigma_t_fixed = legal_cov(Sigma_t); 

[A_t, B_t] = estimate_jacobian(X_t, U_t, dt);
C_t = estimate_observation_jacobian(X_t); 

time_sigma = A_t*Sigma_t_fixed*A_t' + Q; 
K = time_sigma*C_t'/(C_t*time_sigma*C_t' + R);
Innov_t1 = K*C_t*time_sigma; 


end

