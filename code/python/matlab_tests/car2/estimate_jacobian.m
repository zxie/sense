function [ J_A, J_B ] = estimate_jacobian(X, U, dt)
%computes
%J_A = \partial f(X, U)  / \partial X
%J_B = \partial f(X, U)  / \partial U

    eps = 1e-2;
    
    J_A = zeros(length(X)); 
    for i = 1:length(X)
        dx = zeros(length(X), 1);
        dx(i) = eps; 
        
        f_xu = apply_control(X + dx, U, dt);
        J_A(:,i) = f_xu; 
        
        dx(i) = -eps;
        
        f_xu = apply_control(X + dx, U, dt);
        J_A(:,i) = J_A(:,i) - f_xu; 
    end
    J_A = J_A ./ (2*eps); 
    
    J_B = zeros(length(X), length(U));
    for i = 1:length(U)
        du = zeros(length(U), 1);
        du(i) = eps; 
        
        f_xu = apply_control(X, U + du, dt);
        J_B(:,i) = f_xu; 
        
        du(i) = -eps;
        
        f_xu = apply_control(X, U + du, dt);
        J_B(:,i) = J_B(:,i) - f_xu; 
    end
 
    J_B = J_B ./ (2*eps); 

end


