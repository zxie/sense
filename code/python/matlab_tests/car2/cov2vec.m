function [ vec ] = cov2vec( cov )

ind = logical(triu(ones(size(cov))));
vec = cov(ind);

% vec = reshape(cov,size(cov,1)*size(cov,2),1); 


end

