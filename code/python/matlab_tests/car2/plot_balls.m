function [  ] = plot_balls( balls, plot_col)
% plots your balls 

num_balls = length(balls);


for i = 1 : num_balls
    ball_pos = balls{i}{1};
    scatter(ball_pos(1), ball_pos(2), plot_col);
    ball_unc = balls{i}{2}; 
    plot_ellipse(ball_pos, ball_unc, plot_col); 

end

