
clear all; clc;
load ex3; 

% global declarations
global SIZE_EACH_STATE;
global NUM_STATES;
global SIZE_EACH_CONTROL;
global NUM_CONTROLS;
global obstacles;
global beacons;
global observation_length; 
global conf;
global dt; 
global Q;
global R;
global Sigma_0; 
global NUM_SAMPLES; 
global balls; 
global fov_angle; 
global limits; 
global ball_Q;
global ball_R; 

%problem specific parameters

NUM_SQP_ITERS = 11;
NUM_STATES = 30;
NUM_CONTROLS = NUM_STATES - 1;
SIZE_EACH_STATE = 6;
SIZE_EACH_CONTROL = 2;
NUM_TRAJ = 1;
NUM_SAMPLES = 0;
NUM_TEST_TRAJ = 10;
fov_angle = 0.3;
conf = 0.9;
limits = [-0.1 1.3 -0.1 1]; % of plotting

dt = 0.1; 
X_0 = [0; 0; pi/2; 0.03];

Q = 0.000001*eye(4);
Q(3,3) = 0.000000;
Q(4,4) = 0.0000000;
R = 0.0005*eye(3);
R(3,3) = 0.000005;

Sigma_0 = 0.0001*eye(4);
Sigma_0(3,3) = 0.0000001;
Sigma_0(4,4) = 0.0000001;


obst_1{1} = 0.1;
obst_1{2} = [0.3; 0.5];
obst_2{1} = 0.1; 
obst_2{2} = [0.7; 0.6];
obst_3{1} = 0.05; 
obst_3{2} = [0.1; 0.3];
obst_4{1} = 0.05;
obst_4{2} = [0.5; 0.4];

%obstacles{1} = obst_1;
%obstacles{2} = obst_2; 
%obstacles{2} = obst_3;
%obstacles{4} = obst_4;

beac_1{1} = 0.3;
beac_1{2} = 0.8;
% beac_2{1} = 0.8;
% beac_2{2} = -0.1;
beac_2{1} = 1.0;
beac_2{2} = 0.8; 


beacons{1} = beac_1;
beacons{2} = beac_2;


ball_1{1} = [1; 0.4]; % position
ball_1{2} = 0.0005 * eye(2); % sigma_0 (initial uncertainty) 

balls{1} = ball_1; 


ball_Q = 0.00000*eye(length(balls)*2);
ball_R = 0.001 * eye(length(balls)); 

for j = 1:length(balls)
    Sigma_0 = blkdiag(Sigma_0, balls{j}{2});
end

X_0 = [X_0; balls2vec(balls)]; 
Q = blkdiag(Q, ball_Q);
R = blkdiag(R, ball_R); 


%initialize
plot_type = ['r','g','b','m','c'];
% states = zeros(SIZE_EACH_STATE,NUM_STATES);
% controls = zeros(SIZE_EACH_CONTROL,NUM_CONTROLS);
% states(:,1) = X_0; 

% for i = 2:NUM_STATES
%     U = 0.5*rand(2,1);
%     U(2) = 0.1*(U(2)-(.5));
%     controls(:,i-1) = U; 
%     states(:,i) = apply_control(states(:,i-1), U, dt);
% end 

states = dat_states;
controls = dat_ctrls; 


X_N = [balls2vec(balls); states(3:4,NUM_STATES); balls2vec(balls); cov2vec(zeros(SIZE_EACH_STATE))];
% X_N = [states(1:4,NUM_STATES); cov2vec(zeros(SIZE_EACH_STATE))];

% get ready for SCP
figure; 
hold on;
plot_obstacles(obstacles);
axis(limits)

plot_beacons(beacons, limits);

init_traj = states; 
init_controls = controls; 
[trajectories, W] = trajectory_generator(X_0, Sigma_0, controls, Q, NUM_TRAJ); 
traj_ctrls = repmat(init_controls, [1, 1, NUM_TRAJ]);


fb = zeros(SIZE_EACH_CONTROL, size(X_N,1), NUM_CONTROLS); 

for i = 1:NUM_TRAJ
   plot_trajectory(trajectories(:,:,i), controls, 3); 
end


%plot_trajectory(init_traj,init_controls, 1);
%plot_balls(balls, 'y');

all_scp_states = { };
all_scp_controls = { };
all_scp_states{1} = trajectories;
all_scp_controls{1} = traj_ctrls; 
for k = 1 : NUM_SQP_ITERS 

    A_matrices = cell(NUM_TRAJ,1);
    B_matrices = cell(NUM_TRAJ,1); 
    C_matrices = cell(NUM_TRAJ, 1);
    G_A_matrices = cell(NUM_TRAJ,1);
    G_B_matrices = cell(NUM_TRAJ,1); 
    % compute jacobians
    for j = 1:NUM_TRAJ
        A_matrices{j} = cell(NUM_STATES-1,1);
        B_matrices{j} = cell(NUM_STATES-1,1);
        C_matrices{j} = cell(NUM_STATES-1,1);
        for i = 1:NUM_CONTROLS
            if i > 1
                [J_A, J_B] = estimate_dynamics_jacobian(trajectories(:,i,j) - W(:,i-1,j), traj_ctrls(:,i,j), dt);
            else
                [J_A, J_B] = estimate_dynamics_jacobian(trajectories(:,i,j), traj_ctrls(:,i,j), dt);
            end
            A_matrices{j}{i} = J_A;
            B_matrices{j}{i} = J_B;
            C_matrices{j}{i} = dynamics(trajectories(:,i,j), traj_ctrls(:,i,j), dt); 
        end
        [G_A_j, G_B_j] = penalty_jacobian(reshape(trajectories(:,:,j), ((SIZE_EACH_STATE*(SIZE_EACH_STATE+3)/2)) * NUM_STATES, 1), ...
    reshape(controls, SIZE_EACH_CONTROL * NUM_CONTROLS, 1));
%             [G_A_j, G_B_j] = penalty_jacobian(reshape(trajectories(:,:,j), ((SIZE_EACH_STATE*(SIZE_EACH_STATE+1))) * NUM_STATES, 1), ...
%         reshape(controls, SIZE_EACH_CONTROL * NUM_CONTROLS, 1));
        G_A_matrices{j} = G_A_j;

        G_B_matrices{j} = G_B_j; 

    end

    G_A = zeros(0);
    G_B = zeros(0); 

    for j = 1:NUM_TRAJ
        G_A = blkdiag(G_A, G_A_matrices{j});
        G_B = blkdiag(G_B, G_B_matrices{j});
    end

    
    
    [ opt_states, opt_ctrls, opt_traj_ctrls, opt_fb] = solver_cvx(trajectories, controls, traj_ctrls, fb, A_matrices, B_matrices, C_matrices, W, G_A, G_B, X_N);
    %[ opt_states, opt_ctrls, opt_traj_ctrls, opt_fb, opt_shared_controls] = solver_ddp(trajectories, controls, traj_ctrls, fb, A_matrices, B_matrices, C_matrices, W, G_A, G_B, X_N);

%     shooting
%     scp_states = zeros(size(trajectories));
%     
%     for j = 1:NUM_TRAJ
%         scp_states(:,1,j) = trajectories(:,1,j);
%         for i = 2:NUM_STATES
%             U = opt_fb(:,:,i-1) * scp_states(:,i-1,j) + opt_shared_controls(:,i-1);
%             scp_states(:,i,j) = dynamics(scp_states(:,i-1,j), U, dt) + W(:,i-1,j); 
%         end
%     end

    for j = 1:NUM_TRAJ
        scp_states(:,1,j) = trajectories(:,1,j);
        for i = 2:NUM_STATES
            %U = opt_ctrls(:,i-1);
            scp_states(:,i,j) = dynamics(scp_states(:,i-1,j), opt_traj_ctrls(:,i-1,j), dt) + W(:,i-1,j);
        end
    end
    
    %colocation
%      scp_states = opt_states;
    %controls are set below
    
    fb = opt_fb; 
    
    
    if k == NUM_SQP_ITERS
        for j = 1:NUM_TRAJ
            plot_trajectory(scp_states(:,:,j), opt_traj_ctrls(:,:,j), mod(k,5)+1);
            %plot_trajectory(opt_states(:,:,j), opt_traj_ctrls(:,:,j), mod(k,5)+1);

        end
    end
    all_scp_states{k+1} = scp_states;
    all_scp_controls{k+1} = opt_traj_ctrls; 
    %controls = opt_ctrls;
    %states(:,NUM_STATES) = X_N;
    last_trajectories = trajectories; 
    trajectories = scp_states; 
%     for j  = 1 : NUM_TRAJ
%         trajectories(:,NUM_STATES,j) = X_N; 
%     end
    
    traj_ctrls = opt_traj_ctrls; 
    controls = opt_ctrls; 
    %shared_controls = opt_shared_controls; 
    
    
end



%forward fitting feedback matrices
% fb = zeros(SIZE_EACH_CONTROL, SIZE_EACH_STATE, NUM_CONTROLS); 
% shared_controls = zeros(SIZE_EACH_CONTROL, NUM_CONTROLS); 
% act = scp_states; 
% for i = 1:NUM_CONTROLS
%     cs = act(:,i,:);
%     Bcs = cell(NUM_TRAJ,1); 
%     for j = 1:NUM_TRAJ
%         [J_A, J_B] = estimate_jacobian(cs(:,:,j), traj_ctrls(:,i,j), dt); 
%         Bcs{j} = J_B; 
%     end
%     
%     dns = scp_states(:,i+1,:) - W(:,i,:);
%     cvx_begin
%         variable F(SIZE_EACH_CONTROL, SIZE_EACH_STATE)
%         variable shared_control(SIZE_EACH_CONTROL, 1)
%         variable scores(NUM_TRAJ)
%         
%         minimize(sum(scores))
%         
%         for j = 1:NUM_TRAJ
%             %scores(j) >= norm(Bcs{j}*F*cs(:,:,j) - (dns(:,:,j) - apply_control(cs(:,:,j), traj_ctrls(:,i,j), dt)))
%             scores(j) >= norm(Bcs{j}*F*cs(:,:,j) - (dns(:,:,j) - Bcs{j}*(shared_control(:,:)-traj_ctrls(:,i,j)) - apply_control(cs(:,:,j), traj_ctrls(:,i,j), dt)))
%         end
%     cvx_end 
%     
%     fb(:,:,i) = F; 
%     shared_controls(:,i) = shared_control; 
%     
%    
%    for j = 1:NUM_TRAJ
%        %act(:,i+1,j) = apply_control(cs(:,:,j),  fb(:,:,i)*cs(:,:,j), dt) + W(:,i,j); 
%        act(:,i+1,j) = apply_control(cs(:,:,j), shared_controls(:,i) + fb(:,:,i)*cs(:,:,j), dt) + W(:,i,j); 
%    end
%    
%    
% end
% 
% for j = 1:NUM_TRAJ
%     plot_trajectory(act(:,:,j), opt_traj_ctrls(:,:,j), 5);
% end
% 
% return; 



% backward fitting of feedback matrices
% fb = zeros(SIZE_EACH_CONTROL, SIZE_EACH_STATE, NUM_CONTROLS);
% shared_controls = zeros(SIZE_EACH_CONTROL, NUM_CONTROLS); 
% cs = trajectories; 
% cc = traj_ctrls; 
% 
% for t = NUM_CONTROLS:-1:1 % solve for policy at time t 
%     display(t)
%     
%     %compute states under current future policy 
%     % shooting
% %     for j = 1:NUM_TRAJ
% %         for i = t+1:NUM_CONTROLS
% %             cc(:,i,j) = fb(:,:,i)*cs(:,i,j) + shared_controls(:,i);
% %             cs(:,i+1,j) = apply_control(cs(:,i,j), cc(:,i,j), dt) + W(:,i,j);
% %         end
% %     end
%     
%     %colocation
%     if t < NUM_CONTROLS
%         cs(:,t+1:NUM_CONTROLS,:) = opt_x(:,t+1:NUM_CONTROLS,:);
%         cc(:,t+1:NUM_CONTROLS,:) = opt_u(:,t+1:NUM_CONTROLS,:); 
%     end
%     
%     %compute jacobians under current states
%     A_mat = cell(NUM_TRAJ,1);
%     B_mat = cell(NUM_TRAJ,1); 
%     c_mat = cell(NUM_TRAJ,1); 
%     
%     for j = 1:NUM_TRAJ
%         A_mat{j} = cell(NUM_CONTROLS,1); % this is storage inefficient, but uses less of my brain cycles 
%         B_mat{j} = cell(NUM_CONTROLS,1); 
%         c_mat{j} = cell(NUM_CONTROLS,1); 
%         for i = t:NUM_CONTROLS
%             if i == 1
%                [J_A, J_B] = estimate_jacobian(cs(:,i,j), cc(:,i,j), dt);
%             else
%                [J_A, J_B] = estimate_jacobian(cs(:,i,j) - W(:,i-1,j), cc(:,i,j), dt);  
%             end
%             A_mat{j}{i} = J_A;
%             B_mat{j}{i} = J_B; 
%             c_mat{j}{i} = apply_control(cs(:,i,j), cc(:,i,j), dt); 
%         end
%     end
%     
%     
%     %solve the problem 
%     cvx_begin
%         variable F(SIZE_EACH_CONTROL, SIZE_EACH_STATE)
%         variable u_bar(SIZE_EACH_CONTROL)
%         variable opt_x(SIZE_EACH_STATE, NUM_STATES, NUM_TRAJ)
%         variable opt_u(SIZE_EACH_CONTROL, NUM_CONTROLS, NUM_TRAJ)
%         
%         
%         %minimize(norm(opt_u(1,:)) + ...
%         minimize(norm(reshape(opt_x(:,NUM_STATES,:) - repmat(X_N, [1,1,NUM_TRAJ]), SIZE_EACH_STATE*NUM_TRAJ, 1)))
%         
%         for j = 1:NUM_TRAJ
%             opt_x(:,t,j) == cs(:,t,j);
%             opt_u(:,t,j) == F*cs(:,t,j) + u_bar; 
%             for i = t+1:NUM_CONTROLS
%                opt_u(:,i,j) == fb(:,:,i)*opt_x(:,i,j) + shared_controls(:,i); 
%             end
%             
%             for i = t:NUM_CONTROLS
%                opt_x(:,i+1,j) == A_mat{j}{i}*(opt_x(:,i,j) - cs(:,i,j)) + ...
%                                  B_mat{j}{i}*(opt_u(:,i,j) - cc(:,i,j)) + ...
%                                  c_mat{j}{i} + W(:,i,j); 
%             end
%             abs(u_bar) < 1.0
%             %abs(opt_x(:,:,j) - cs(:,:,j)) <= 0.05; 
%         end
%         
%     
%     cvx_end
%     
%     fb(:,:,t) = F;
%     shared_controls(:,t) = u_bar; 
%     
%     
% end
% return; 


return;

%globally fitting feedback matrices and shared controls 
B_new_matrices = cell(NUM_TRAJ,1); 
% compute jacobians
for j = 1:NUM_TRAJ
    B_new_matrices{j} = cell(NUM_STATES-1,1);
    for i = 1:NUM_CONTROLS
        if i > 1
            [J_A, J_B] = estimate_dynamics_jacobian(trajectories(:,i,j) - W(:,i-1,j), traj_ctrls(:,i,j), dt);
        else
            [J_A, J_B] = estimate_dynamics_jacobian(trajectories(:,i,j), traj_ctrls(:,i,j), dt);
        end
        B_new_matrices{j}{i} = J_B;
    end
end


cvx_begin
    variable fb_1(SIZE_EACH_CONTROL, SIZE_EACH_STATE, NUM_CONTROLS)
    variable shared_controls(SIZE_EACH_CONTROL, NUM_CONTROLS)
    variable penalty_scores(NUM_TRAJ, NUM_CONTROLS)

    minimize(sum(sum(penalty_scores)))

    for j = 1:NUM_TRAJ
        for i = 1:NUM_CONTROLS
            penalty_scores(j,i) >= norm(trajectories(:,i+1,j) - dynamics(trajectories(:,i,j), traj_ctrls(:,i,j), dt) - W(:,i,j) + ...
                B_new_matrices{j}{i}*(traj_ctrls(:,i,j) - fb_1(:,:,i)*trajectories(1:SIZE_EACH_STATE,i,j) - shared_controls(:,i)));
            %B_new_matrices{j}{i}*(traj_ctrls(:,i,j) - fb_1(:,:,i)*trajectories(:,i,j)));
            

        end
        abs(shared_controls(:,:)) < 1.0; 
    end


cvx_end
fb = fb_1;
return; 


% test 
test_trajectories = zeros(size(trajectories,1), size(trajectories,2), NUM_TEST_TRAJ); 
test_traj_ctrls = zeros(SIZE_EACH_CONTROL, NUM_CONTROLS, NUM_TEST_TRAJ); 
for j = 1:NUM_TEST_TRAJ
        test_trajectories(:,1,j) = [mvnrnd(X_0, Sigma_0(1:4, 1:4))'; cov2vec(Sigma_0)]; 
        
        for i = 2:NUM_STATES
            %U = opt_ctrls(:,i-1);
            w = [mvnrnd(zeros(SIZE_EACH_STATE,1), Q(1:4,1:4))'; cov2vec(zeros(SIZE_EACH_STATE))];  
            test_traj_ctrls(:,i-1,j) = shared_controls(:,i-1) + fb(:,:,i-1)*test_trajectories(:,i-1,j); 
            %test_traj_ctrls(:,i-1,j) = controls(:,i-1) + fb(:,:,i-1)*w; 
            test_trajectories(:,i,j) = dynamics(test_trajectories(:,i-1,j), test_traj_ctrls(:,i-1,j) , dt) + w;
        end
        
        plot_trajectory(test_trajectories(:,:,j), test_traj_ctrls(:,:,j), 6);      
end


train_trajectories = zeros(size(trajectories)); 
train_traj_ctrls = zeros(SIZE_EACH_CONTROL, NUM_CONTROLS, NUM_TRAJ); 
for j = 1:NUM_TRAJ
        train_trajectories(:,1,j) = trajectories(:,1,j); 
        
        for i = 2:NUM_STATES
            %U = opt_ctrls(:,i-1);
            %w = mvnrnd(zeros(SIZE_EACH_STATE,1), Q(1:4,1:4))'; 
            train_traj_ctrls(:,i-1,j) = shared_controls(:,i-1) + fb(:,:,i-1)*train_trajectories(:,i-1,j); 
            %train_traj_ctrls(:,i-1,j) = controls(:,i-1) + fb(:,:,i-1)*W(:,i-1,j);
            train_trajectories(:,i,j) = dynamics(train_trajectories(:,i-1,j), train_traj_ctrls(:,i-1,j) , dt) + W(:,i-1,j);
        end
        
        plot_trajectory(train_trajectories(:,:,j), train_traj_ctrls(:,:,j), 3); 
        
        
end







