function [ J_A, J_B ] = penalty_jacobian( X, U )

global SIZE_EACH_STATE; 
%computes
%J_A = \partial g(X, U)  / \partial X
%J_B = \partial g(X, U)  / \partial U



    eps = 1e-2;
    
    J_A = zeros(1, length(X)); 
    for i = 1:length(X)
        if i > SIZE_EACH_STATE
            eps = 1e-5; 
        end
        dx = zeros(length(X), 1);
        dx(i) = eps; 
        
        g_xu = penalty(X + dx, U);
        J_A(:,i) = g_xu; 
        
        dx(i) = -eps;
        
        g_xu = penalty(X + dx, U);
        J_A(:,i) = J_A(:,i) - g_xu; 
        J_A(:,i) = J_A(:,i) ./ (2*eps); 
    end
    
    eps = 1e-2; 
    
    
    J_B = zeros(1, length(U));
    for i = 1:length(U)
        du = zeros(length(U), 1);
        du(i) = eps; 
        
        g_xu = penalty(X, U + du);
        J_B(:,i) = g_xu; 
        
        du(i) = -eps;
        
        g_xu = penalty(X, U + du);
        J_B(:,i) = J_B(:,i) - g_xu; 
        J_B(:,i) = J_B(:,i) ./ (2*eps); 

    end
 
end

