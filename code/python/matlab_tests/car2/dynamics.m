function [ Y ] = dynamics( X, U, dt )
% simulates the dynamics of the system and propogates the covariance

global SIZE_EACH_STATE; 

X_s = X(1:SIZE_EACH_STATE);
Y_s = apply_control(X_s, U, dt); 

X_c = X(SIZE_EACH_STATE+1:end);
cov = vec2cov(X_c);

cov_up = cov_dynamics(X_s, U, cov);

Y_c = cov2vec(cov_up);

Y = [Y_s; Y_c]; 


end

