function [ c ] = penalty( X_flat, U_flat )

% penalty function is on the trajectory
    
    global SIZE_EACH_STATE;
    global NUM_STATES;
    global SIZE_EACH_CONTROL;
    global NUM_CONTROLS;
    global obstacles; 
    global Sigma_0;
    global NUM_SAMPLES;
    global Q;
    global R; 
    global balls;
    global ball_Q;
    global ball_R; 


    U = reshape(U_flat, SIZE_EACH_CONTROL, NUM_CONTROLS);
    X = reshape(X_flat, SIZE_EACH_STATE*(SIZE_EACH_STATE+3)/2, NUM_STATES);
    %X = reshape(X_flat, SIZE_EACH_STATE + SIZE_EACH_STATE*SIZE_EACH_STATE, NUM_STATES);

    
%     sigmas = kalman_sigma(X, U, Sigma_0, Q, R); 
%     ball_sigmas = kalman_sigma_balls(X, ball_Q, ball_R); 
    c = 0; 
    
    %c = norm(X(3,:)); 
    c = c + norm(U(2,:));
    %c = c + norm(diff(U'));
    
    for j = 1:length(obstacles) 
        obst = obstacles{j};
        for i = 1:NUM_STATES
            [X_s, X_c] = split_state(X(:,i));
            mu = X_s;
            sig = legal_cov(vec2cov(X_c)); 
            sample = mvnrnd(mu,sig,NUM_SAMPLES)'; 

            for k = 1:NUM_SAMPLES
                if ( norm(sample(1:2,k) - obst{2}) < obst{1} + 0.01 )
                    c = c + 2 + obst{1} + 0.01 - norm(sample(1:2,k) - obst{2});
                end
            end
%             if ( norm(mu - obst{2}) < obst{1} + 0.01 )
%                 c = c + 2 + obst{1} + 0.01 - norm(mu - obst{2});
%             end
        end
    end
    
%     for j = 1:length(NUM_STATES)
%        [X_s, X_c] = split_state(X(:,j));
%        cov = vec2cov(X_c); 
%        c = c + trace(cov); 
%     end

          
%     for j = 1:length(obstacles) 
%         obst = obstacles{j};
%         for i = 1:NUM_STATES 
%             mu = X(:,i); 
%             sigma = sigmas{i};
%             sample = mvnrnd(mu,sigma(1:4,1:4),NUM_SAMPLES)'; 
%             
%             for k = 1:NUM_SAMPLES
%                 if ( norm(sample(1:2,k) - obst{2}) < obst{1} + 0.01 )
%                     c = c + 2 + obst{1} + 0.01 - norm(sample(1:2,k) - obst{2});
%                 end
%             end
%         end
%     end
%     
%     for j = 1:length(sigmas)
%         %c = c + log(sqrt((2*pi*exp(1))^4*det(sigmas{j})));
%         c = c + trace(sigmas{j});
%         %sig = sigmas{j};
%         %ball_sigmas = sig(5:6, 5:6); 
%         %c = c + 10*trace(ball_sigmas); 
%     end
%     
%     for j = 1:length(ball_sigmas)
%         %c = c + log(sqrt((2*pi*exp(1))^2*det(ball_sigmas{j})));
%         c = c + 3*trace(ball_sigmas{j});
%     end

  
    
end

