function [ X, W ] = trajectory_generator( x_0, Sigma_0, U, Q, N )
% Given the mean initial state, x_0, and uncertainty ellipsoid \Sigma_0, 
% we will generate N trajectories using controls U and noise sampled
% from distribution Q. 

global SIZE_EACH_STATE; 
global NUM_STATES; 
global NUM_CONTROLS; 
global dt; 

X = zeros(size(x_0,1) + size(cov2vec(Sigma_0),1), NUM_STATES, N); 
W = zeros(size(X)); 

% X = zeros(SIZE_EACH_STATE + (SIZE_EACH_STATE)*(SIZE_EACH_STATE), NUM_STATES, N); 
% W = zeros(SIZE_EACH_STATE + (SIZE_EACH_STATE)*(SIZE_EACH_STATE), NUM_STATES, N); 


for i = 1:N
    %W(:,1,i) = zeros(SIZE_EACH_STATE,1);
    %W(:,1,i) = mvnrnd(zeros(SIZE_EACH_STATE,1), Sigma_0(1:4,1:4))'; 
    %init_noise = mvnrnd(zeros(SIZE_EACH_STATE,1), Sigma_0)';
    init_noise = zeros(SIZE_EACH_STATE,1);
    %init_noise(5:6) = 0; 
    X(:,1,i) =  [x_0 + init_noise; cov2vec(Sigma_0)]; 
end


for j = 1:N 
    for i = 1:NUM_CONTROLS
        %W(:,i+1,j) = zeros(SIZE_EACH_STATE,1);
        %W(:,i+1,j) = mvnrnd(zeros(SIZE_EACH_STATE,1), Q(1:4,1:4))'; 

        %W(1:SIZE_EACH_STATE,i,j) = mvnrnd(zeros(SIZE_EACH_STATE,1), Q)'; 
        
        X(:,i+1,j) = dynamics(X(:,i,j), U(:,i), dt) + W(:,i,j);

    end
end



end

