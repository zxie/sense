function [ cov ] = vec2cov( vec )
% length(vec) = n(n+1)/2

n = (sqrt(1+8*length(vec)) - 1) / 2;
sparsity_pattern = logical(triu(ones(n,n)));
cov = zeros(n,n);
cov(sparsity_pattern) = vec;
cov = cov + triu(cov,1)'; 
% 
% n = sqrt(length(vec)); 
% cov = reshape(vec,n,n); 





end

