function [ rx, ry ] = plot_car( X, plot_type_ind )
%plots the car on the current window
plot_type = ['r','g','b','m','c', 'y'];


x = X(1);
y = X(2);
theta = X(3);

B = [0 0 0.1 0.1; -0.05 0.05 0.05 -0.05;];
B = B .* 0.1;
R = [cos(theta) -sin(theta);
     sin(theta) cos(theta)];
rot_box = R * B;

rx = rot_box(1,:);
ry = rot_box(2,:);
patch(rx+x,ry+y,plot_type(1,plot_type_ind));

end

