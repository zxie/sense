function [ lcov ] = legal_cov( cov )


[V,D] = eig(cov);
for i = 1:length(D)
    if D(i,i) < 0
        D(i,i) = 0; 
    end
end
lcov = V*D*V'; 

end

