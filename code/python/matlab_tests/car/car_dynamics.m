function [ Y ] = car_dynamics(X, U, dt)
% Applies control U to state X for time dt. Returns the state in Y. 
% 
L = 0.01; %% set the length of the car

x = X(1);
y = X(2);
theta = X(3); 
v = X(4); 

u_a = U(1);
u_phi = U(2);



x_dot = v * cos(theta);
y_dot = v * sin(theta);
theta_dot = v * tan(u_phi) / L;
v_dot = u_a; 

X_dot = [x_dot; y_dot; theta_dot; v_dot;]; 

Y = X + dt * X_dot; 

% A = eye(4);
% B = [0.1 0.2; -0.3 0.5; 0.5 -0.1; 1.0 0.2];
% 
% Y = dt*A*X + dt*B*U; 


end

