clear all ; close all; 

NX = 4;
NU = 2;
T = 20;
N_iter = 1;
dt = 0.1;

W = 0.000001 * eye(4); 


limits = [-0.5, 0.5, -0.5, 0.5];
figure; hold on;
axis(limits); 

x0 = [0; 0; 0; 0.1];
X_bar = zeros(NX, T+1);
U_bar = rand(NU, T)/3;
U_bar(:,T/2:end) = -2 * U_bar(:,T/2:end);


X_bar(:,1) = x0;
plot_car(X_bar(:,1),1);
for t = 1:T
    X_bar(:,t+1) = car_dynamics(X_bar(:,t), U_bar(:,t), dt);
    plot_car(X_bar(:,t+1),1);
end

X = X_bar;
U = U_bar;

for iter = 1:N_iter
    
    As = zeros(NX, NX, T);
    Bs = zeros(NX, NU, T);
    Cs = zeros(NX, 1, T);
    Ws = zeros(NX, NX, T);

    for t = 1:T
       [As(:,:,t), Bs(:,:,t)] = estimate_car_dynamics_jacobian(X(:,t), U(:,t), dt);
       Cs(:,:,t) = car_dynamics(X(:,t), U(:,t), dt) - As(:,:,t)*X(:,t) - Bs(:,:,t)*U(:,t);
       Ws(:,:,t) = W; 
    end

    rho_x = 0.1; 
    rho_u = 0.1; 

    [X_opt, U_opt, K, u0] = solver_cvx_feedback(X, U, As, Bs, Cs, Ws, 0, rho_x, rho_u, X_bar(:,T+1), NX);
    
    X(:,1) = x0;
    for t = 1:T 
        U(:,t) = K(NU*(t-1)+1:NU*t,1:NX*t)  * reshape(X(:,1:t), NX*t, 1) + u0(NU*(t-1)+1:NU*t); 
        X(:,t+1) = car_dynamics(X(:,t), U(:,t), dt);
    end
    
end


X(:,1) = x0;
plot_car(X(:,1),1);
for t = 1:T
    U(:,t) = K(NU*(t-1)+1:NU*t,1:NX*t)  * reshape(X(:,1:t), NX*t, 1) + u0(NU*(t-1)+1:NU*t); 
    X(:,t+1) = car_dynamics(X(:,t), U(:,t), dt);
    plot_car(X(:,t+1),2);
end

stop

for j = 1:10; 
    for t = 1:T
    %U(:,t) = K(NU*(t-1)+1:NU*t,1:NX*t)  * reshape(X(:,1:t), NX*t, 1) + u0(NU*(t-1)+1:NU*t); 
    X(:,t+1) = car_dynamics(X(:,t), U(:,t), dt) + mvnrnd(zeros(NX,1)',W)';
    plot_car(X(:,t+1),3);
    end
end