function [ lcov ] = legal_hessian( cov )


[V,D] = eig(cov);
for i = 1:length(D)
    if D(i,i) < 0.0
        D(i,i) = 0.0; 
    end
end
lcov = V*D*V'; 

end

