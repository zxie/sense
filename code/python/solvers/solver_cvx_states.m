% With beliefs/covariances incorporated probably won't be using
% this file anymore
function [opt_states, opt_ctrls cvx_optval] = ...
    solver_cvx_states(X_bar, U_bar, As, Bs, Cs, rho_x, rho_u)

%TODO Incorporate additional dimension of trajectories
%TODO Incorporate noise

SIZE_STATE = size(X_bar, 1);
NUM_STATES = size(X_bar, 2);
SIZE_CTRL = size(U_bar, 1);
NUM_CTRLS = NUM_STATES - 1; 

cvx_solver sdpt3 % sdpt3/sedumi

cvx_begin

    variable opt_states(SIZE_STATE, NUM_STATES)
    variable opt_ctrls(SIZE_CTRL, NUM_CTRLS)

    minimize norm(opt_states(:,NUM_STATES) - X_bar(:,NUM_STATES), 2) + ... % Get close to goal
             0.01*sum(sum(opt_ctrls.*opt_ctrls)) % And minimize controls applied

    subject to

        % Start and end constraints
        opt_states(:,1) == X_bar(:,1)
        % opt_states(:,NUM_STATES) == X_bar(:,NUM_STATES)

        % Dynamics constraints
        for t=2:NUM_STATES
            opt_states(:,t) == As(:,:,t-1)*(opt_states(:,t-1) - X_bar(:,t-1)) + ...
                               Bs(:,:,t-1)*(opt_ctrls(:,t-1) - U_bar(:, t-1)) + ...
                               Cs(:,t-1)

        end

        % Trust region constraints
        abs(opt_states - X_bar) <= rho_x
        abs(opt_ctrls - U_bar) <= rho_u

cvx_end

end

