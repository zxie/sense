function [opt_bels, opt_ctrls, cvx_optval] = ...
    scp_solver_cvx(Bel_bar, U_bar, As, Bs, Cs, J_A, rho_bel, rho_u, goal_bel, nx)

%TODO Incorporate additional dimension of trajectories
%TODO Incorporate noise

SIZE_BEL = size(Bel_bar, 1);
NUM_BELS = size(Bel_bar, 2);
SIZE_CTRL = size(U_bar, 1);
NUM_CTRLS = NUM_BELS - 1; 
%nx = 4 %FIXME

cvx_solver sdpt3 % sdpt3/sedumi

cvx_begin

    variable opt_bels(SIZE_BEL, NUM_BELS)
    variable opt_ctrls(SIZE_CTRL, NUM_CTRLS)
    variable T(NUM_BELS-1)

    % Get close to goal belief (e.g. Sigma=0)
    %minimize 10*norm(opt_bels(1:nx,NUM_BELS) - goal_bel(1:nx), 2) + ...
            %0.001*norm(opt_ctrls(:), 2) + 100*trace(reshape(opt_bels(nx+1:end, end), nx, nx))
            minimize 1000*norm(opt_bels(1:nx,NUM_BELS) - goal_bel(1:nx), 2) + 5000*sum(T(end)) + 0.001*sum(sum(opt_ctrls.*opt_ctrls)) + 50.0*reshape(J_A,1,SIZE_BEL*NUM_BELS)*reshape(opt_bels, SIZE_BEL*NUM_BELS, 1) 
    subject to

        % Start constraint
        opt_bels(:,1) == Bel_bar(:,1)

        % Dynamics constraints
        for t=2:NUM_BELS
            opt_bels(:,t) == As(:,:,t-1)*(opt_bels(:,t-1) - Bel_bar(:,t-1)) + ...
                               Bs(:,:,t-1)*(opt_ctrls(:,t-1) - U_bar(:, t-1)) + ...
                               Cs(:,t-1)
            %reshape(opt_bels(nx+1:end,t), nx, nx) == semidefinite(nx)
            T(t-1) >= trace(reshape(opt_bels(nx+1:end, t), nx, nx))
            T(t-1) >= 0
            %FIXME TODO Set J'x - J'x_bar + f_col(x_bar) \ge 0
            %reshape(J_A,1,SIZE_BEL*NUM_BELS)*reshape(opt_bels, SIZE_BEL*NUM_BELS, 1) >= 0
        end

        % Trust region constraints
        abs(opt_bels - Bel_bar) <= rho_bel
        abs(opt_ctrls - U_bar) <= rho_u

cvx_end

end

