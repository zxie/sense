% With beliefs/covariances incorporated probably won't be using
% this file anymore
function [X, U, cvx_optval] = ...
    solver_cvx_states(X_bar, U_bar, As, Bs, Cs, Hs, Js, Gs, Hf, Jf, Gf, rho_x, rho_u)

%TODO Incorporate additional dimension of trajectories
%TODO Incorporate noise

SIZE_STATE = size(X_bar, 1);
NUM_STATES = size(X_bar, 2);
SIZE_CTRL = size(U_bar, 1);
NUM_CTRLS = NUM_STATES - 1;
T = NUM_STATES; 

for t = 1:T-1
    Hs(:,:,t) = legal_hessian(Hs(:,:,t));
end
Hf = legal_hessian(Hf);

cvx_solver sdpt3 % sdpt3/sedumi

cvx_begin 

    variable X(SIZE_STATE, NUM_STATES)
    variable U(SIZE_CTRL, NUM_CTRLS)
    variable ct(NUM_STATES)

    minimize sum(ct)

    subject to

        % Start and end constraints
        X(:,1) == X_bar(:,1)
        %X(:,T) == X_bar(:,T)
        % opt_states(:,NUM_STATES) == X_bar(:,NUM_STATES)

        % Dynamics constraints
        for t=1:T-1
            X(:,t+1) == As(:,:,t)*(X(:,t) - X_bar(:,t)) + ...
                                 Bs(:,:,t)*(U(:,t) - U_bar(:, t)) + ...
                                 Cs(:,t)
            ct(t) >= 0.5 * [X(:,t) - X_bar(:,t); U(:,t) - U_bar(:,t)]' * Hs(:,:,t) * [X(:,t) - X_bar(:,t); U(:,t) - U_bar(:,t)] + ...
                     Js(:,t)' * [X(:,t) - X_bar(:,t); U(:,t) - U_bar(:,t)] + Gs(:,t)
        end

        ct(NUM_STATES) >= 0.5 * (X(:,T) - X_bar(:,T))'*Hf*(X(:,T)-X_bar(:,T)) +  Jf*(X(:,T)-X_bar(:,T)) + Gf

        % Trust region constraints
        abs(X - X_bar) <= rho_x
        abs(U - U_bar) <= rho_u
        abs(U) <= 0.2

cvx_end

end

