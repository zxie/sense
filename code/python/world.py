import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#NOTE This file no longer used

#TODO This wrapper might not even be necessary, just have SimEnv be the
# highest level of abstraction

class World:

    world_index = 0

    @classmethod
    def increment_index(cls):
        cls.world_index += 1
        return cls.world_index

class World2D(World):
    
    def __init__(self, robots, sim_env):
        self.robots = robots
        self.sim_env = sim_env
        self.index = World2D.increment_index()

    def __str__(self):
        return 'world_2d[' + str(self.index) + ']'

    def display(self):
        plt.figure()
        ax = plt.axes()
        ax.set_aspect('equal')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        plt.title(str(self))
        self.sim_env.draw()
        for r in self.robots:
            r.draw()

class World3D(World):
    
    def __init__(self, robots, sim_env):
        self.robots = robots
        self.sim_env = sim_env
        self.index = World3D.increment_index()

    def __str__(self):
        return 'world_3d[' + str(self.index) + ']'

    def display(self):
        fig = plt.figure()
        ax = Axes3D(fig)
        ax.set_aspect('equal')
        plt.title(str(self))
        for r in self.robots:
            r.draw(ax)
        self.sim_env.draw(ax)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')


