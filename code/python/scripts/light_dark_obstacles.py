import os, sys
sys.path.append(os.path.abspath('..'))
import numpy as np
from sim_env import SimEnv2D, Beacon, RectangularObstacle, CircularObstacle
import matplotlib.pyplot as plt
import time

env = SimEnv2D(beacons=[Beacon(np.array([0, 0])),\
                        Beacon(np.array([1, 1]))])
obs1 = RectangularObstacle(np.array([[-0.1, 0.1], [0.1, 0.1], [0.1, -0.1], [-0.1, -0.1]]).T)
obs2 = CircularObstacle(0.5, 0.5, 0.1)
env.add_obstacle(obs1)
env.add_obstacle(obs2)
env.draw()

#pts = np.random.random((2,2))
pts = np.array([[0, 0.5], [0, 0.9]])

start = time.time()
print obs1.in_collision(pts)
end = time.time()
print end-start
start = time.time()
print obs2.in_collision(pts)
end = time.time()
print end-start
plt.plot(pts[0,0], pts[1,0], 'bo')
plt.plot(pts[0,1], pts[1,1], 'go')
print env.in_collision(pts)

plt.show()

