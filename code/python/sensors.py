from numpy import *
from numpy.linalg import norm
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon, PathPatch
from matplotlib.path import Path
from matplotlib.collections import PatchCollection
from matplotlib.transforms import Affine2D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from transforms import *
from utils import *
try:
  import openravepy 
except ImportError:
  print 'Cannot import openrave; skipping'

#TODO Make sensors fit the SimObj template?
class Sensor:
    
    NZ = -1 # Size of observation

    sensor_index = 0

    @classmethod
    def increment_index(cls):
        cls.sensor_index += 1
        return cls.sensor_index

    def move_to(self, x, y, theta):
        raise Exception('move_to unimplemented')

    def observe(self, scene, x=None):
        # Take in a scene and return an observation vector z
        # Can then add noise term to z
        raise Exception('observe unimplemented')

    # Assuming Gaussian noise don't have to write linearize--already
    # implemented in ekf_update

    def copy(self):
        #TODO
        raise Exception('copy unimplemented')

    def draw(self):
        raise NotImplementedError()

class BeaconSensor(Sensor):

    # Creates light-dark environment for localizing robots
    # Measurement decays as 1/(distance squared from beacon)

    NZ = -1 # Depends on number of beacons

    def __init__(self, x=0, y=0, decay_coeff=25):
        self.x = x
        self.y = y
        self.decay_coeff = decay_coeff # Somewhat arbitary, tune so reasonable

    def move_to(self, x, y):
        self.x = x
        self.y = y

    def observe(self, sim_env, x):
        # Give an observation whose magnitude decays based on distance
        # from beacon
        if x == None:
            x = np.array([self.x, self.y])
        beacons = sim_env.beacons
        z = np.mat(zeros((len(beacons), 1)))
        for k in xrange(len(beacons)):
            denom = scalar(self.decay_coeff*(x[0]-beacons[k].pos[0])**2 +\
                      self.decay_coeff*(x[1]-beacons[k].pos[1])**2 + 1)
            z[k] = 1/denom
        return z

    def draw(self):
        pass

class BeaconSensor3D(Sensor):

    # Creates light-dark environment for localizing robots
    # Measurement decays as 1/(distance squared from beacon)

    NZ = -1 # Depends on number of beacons

    def __init__(self, x=0, y=0, z=0, decay_coeff=25):
        self.x = x
        self.y = y
        self.z = z
        self.decay_coeff = decay_coeff # Somewhat arbitary, tune so reasonable

    def observe(self, sim_env, x):
        # Give an observation whose magnitude decays based on distance
        # from beacon
        if x == None:
            x = np.array([self.x, self.y, self.z])
        beacons = sim_env.beacons
        z = np.mat(zeros((len(beacons), 1)))
        for k in xrange(len(beacons)):
            denom = scalar(self.decay_coeff*(x[0]-beacons[k].pos[0])**2 +\
                      self.decay_coeff*(x[1]-beacons[k].pos[1])**2 +\
                      self.decay_coeff*(x[2]-beacons[k].pos[2])**2 + 1)
            z[k] = 1/denom
        return z

    def draw(self):
        pass


class FOVSensor(Sensor):

    # Localizes objects with strength of reading based on whether object
    # is in field of view

    NZ = -1 # Depends on number of points

    def __init__(self, x, fov_angle=np.pi/4, decay_coeff=5):
        self.x = x # x, y, theta
        self.fov_angle = fov_angle
        self.decay_coeff = decay_coeff # Somewhat arbitrary, tune so reasonable

    def move_to(self, x, y, theta):
        self.x[0] = x
        self.x[1] = y
        self.x[2] = theta

    def observe(self, sim_env, x=None):
        # Give an observation whose magnitude decays based on distance
        # from object centers and whether object is in field of view
        # Smooth field-of-view decay
        # x[0] = x
        # x[1] = y
        # x[2] = theta
        # x[3] = obj_1 x  
        # x[4] = obj_1 y
        # etc

        if x == None:
          print 'well fuck'
          stop
        x = np.array(x.T).ravel()
        num_objs = (len(x) - 3) / 2
        z = np.mat(zeros((num_objs, 1)))
        rotation = np.mat([[cos(x[2]), -sin(x[2])], [sin(x[2]), cos(x[2])]])
        sensor_dir = np.mat([[cos(x[2]), -sin(x[2])], [sin(x[2]), cos(x[2])]])*np.mat('1; 0')
        sensor_pos = np.mat([[x[0]], [x[1]]])
        for idx in range(num_objs):
            obj_x = np.mat(x[3+2*idx:3+2*(idx+1)]).T
            # distance
            denom_dist = scalar(self.decay_coeff*(x[0]-obj_x[0])**2 +\
                      self.decay_coeff*(x[1]-obj_x[1])**2 + 1)
            sensor_to_obj = obj_x[0:2].reshape(sensor_pos.shape) - sensor_pos
            dprod = sensor_dir.T*np.mat(sensor_to_obj)
            assert dprod.shape[0] == 1 and dprod.shape[1] == 1
            cos_ang = scalar(dprod)/norm(sensor_to_obj,2)
            if (cos_ang >= np.cos(self.fov_angle/2)):
                # Inside field of view, give decaying signal
                #print cos_ang
                signal = math.exp(-50*(cos_ang - 1)**2)
                #signal = (cos_ang - np.cos(self.fov_angle/2))
                z[idx] = signal / denom_dist
                #print z[idx]
        return z
 
    '''
    def observe(self, sim_env, x=None):
        # Give an observation whose magnitude decays based on distance
        # from object centers and whether object is in field of view
        # Smooth field-of-view decay
        if x == None:
            x = self.x
        objs = sim_env.objects
        z = np.mat(zeros((len(objs), 1)))
        sensor_dir = np.mat([[cos(x[2]), -sin(x[2])], [sin(x[2]), cos(x[2])]])*\
                         np.mat('1; 0')
        sensor_pos = np.mat([[x[0]], [x[1]]])
        for idx, obj in enumerate(objs):
            # distance
            denom_dist = scalar(self.decay_coeff*(x[0]-obj.x[0])**2 +\
                      self.decay_coeff*(x[1]-obj.x[1])**2 + 1)
            sensor_to_obj = obj.x[0:2].reshape(sensor_pos.shape) - sensor_pos
            dprod = sensor_dir.T*np.mat(sensor_to_obj)
            assert dprod.shape[0] == 1 and dprod.shape[1] == 1
            cos_ang = scalar(dprod)/norm(sensor_to_obj,2)
            if (cos_ang >= np.cos(self.fov_angle/2)):
                # Inside field of view, give decaying signal
                signal = math.exp(-500*(cos_ang - 1)**2)
                z[idx] =  signal / denom_dist
        return z
    '''

    def draw(self, x=None, color='k'):
        pass


class PinholeCamera2D(Sensor):

    #PIXELS_PER_M = 10000 # Varies of course
    PIXELS_PER_M = 1 #FIXME
    
    def __init__(self, f, x, w, diameter=0.018, \
            image_distance_ratio=1.5, color='k'):
        self.f = f # In pixels
        self.x = x # x[0] and x[1] in m
        self.width = w # In pixels
        self.diameter = diameter # In m
        self.color = color
        #TODO Better way to do image distance probably
        self.image_distance = self.f*image_distance_ratio/self.PIXELS_PER_M
        self.compute_intrinsics()
        self.compute_extrinsics()
        self.index = PinholeCamera2D.increment_index()

    def __str__(self):
        return 'pinhole_camera_2d[' + str(self.index) + ']'

    def observe(self, sim_env, x):
        if x == None:
            x = self.x

        self.move_to(x[0], x[1], x[2])
        zs = list()
    
        #FIXME currently just putting object position in LocalizerBot state
        #for obj in sim_env.objects:
            #zs.append(self.project_point(obj.x[0:2]))
        zs.append(self.project_point(x[3:5]))
     
        return np.mat(zs).T

    def move(self, dx, dy, dtheta):
        self.x[0] += dx
        self.x[1] += dy
        self.x[2] += dtheta
        self.compute_extrinsics()

    def move_to(self, x, y, theta):
        self.x[0] = x
        self.x[1] = y
        self.x[2] = theta
        self.compute_extrinsics()

    def view(self, scene):
        plt.figure()
        w = self.width
        plt.axes().set_xlim([-w/2.0, w/2.0])
        plt.axes().set_ylim([-w/100.0, w/100.0])
        plt.axes().yaxis.set_visible(False)
        plt.axes().set_aspect('equal')
        plt.title(', '.join([str(self), str(scene)]))
        for obj in scene.objects:
            u = self.project_point(obj.x) #TODO
            if abs(u) > w/2.0:
               continue 
            plt.plot(u, 0, 'bo')
            plt.text(u, 0, str(obj.index))

    def project_point(self, pt):
        if len(pt.shape) < 2 or pt.shape[1] != 1:
            pt = np.mat(pt).T

        u_hmg = self.intrinsics_matrix*self.extrinsics_matrix*vstack((pt, 1))
        if allclose(u_hmg, mat('0; 0')):
            raise Exception('Object to project at focal point of camera.')
        if u_hmg[1] == 0:
            return inf
        u = u_hmg[0]/u_hmg[1]
        u = scalar(u)
        return u

    def compute_intrinsics(self):
        self.intrinsics_matrix = matrix([[0, self.f, 0], [1, 0, 0]])

    def compute_extrinsics(self):
        R = theta2R(-self.x[2])
        t = -R*matrix([[self.x[0]], [self.x[1]]])
        self.extrinsics_matrix = matrix(vstack((hstack((R, t)),
                                                array([0, 0, 1]))))

    def draw(self, scale=0.1):
        f = width = scale
        verts = array([[-width/2.0, f], [0, 0],
            [width/2.0, f]])/10.0
        trans = Affine2D().rotate(self.x[2]-pi/2).translate(self.x[0], self.x[1])
        verts = trans.transform(verts)
        triangle = Polygon(verts, False, facecolor='none',
                           edgecolor=self.color)

        curve = Path(array([[-width/2.0, f/2.0], [0, f/1.2],
            [width/2.0, f/2.0]])/10.0,
            codes=[Path.MOVETO]+[Path.CURVE3]*2)
        curve = trans.transform_path(curve)

        ax = plt.gca()
        #ax.plot([self.x[0]], [self.x[1]], 'bo')
        ax.add_patch(triangle)
        ax.add_patch(PathPatch(curve, facecolor='none', edgecolor=self.color))
        #ax.annotate(str(self.index), xy=[self.x[0], self.x[1]])
        #ax.text(self.x[0], self.x[1], str(self.index))


class ExtendedCamera2D(PinholeCamera2D):
    # PinholeCamera2D with radial distortion, depth-of-field, and field of
    # view considerations added
    
    def __init__(self, f, x, w, ks, radial_distortion=True,
                 depth_of_field=True, field_of_view=True, color='k'):
        PinholeCamera2D.__init__(self, f, x, w, color=color)
        self.kappa1 = ks[0]
        self.kappa2 = ks[1]
        self.apply_distortion = radial_distortion
        self.apply_dof = depth_of_field
        self.apply_fov = field_of_view
    
    def project_point(self, pt):
        u_ideal = PinholeCamera2D.project_point(self, pt)

        # Apply radial distortion, only up to 4th order term
        if self.apply_distortion:
            r = u_ideal/self.f
            u_hat = (u_ideal/self.f)*(1 + self.kappa1*r**2 + self.kappa2*r**4)
            u_hat = u_hat*self.f
        else:
            u_hat = u_ideal

        #TODO Impose probabilistic FOV constraints
        if self.apply_fov:
            if abs(u_hat) > self.width/2:
                #FIXME Hack to get Kalman linearization to 0
                return u_hat/abs(u_hat)*self.width/2

        #TODO Better way to apply circle of confusion?
        if self.apply_dof:
            z = 1/(1/(self.f/self.PIXELS_PER_M) - 1/pt[0])
            z = scalar(z)
            d = self.diameter*(z - self.image_distance)/z
            d = abs(d); # Blur circle diameter in m
            #print 'Blur circle diameter: ' + str(d)
            # Just 2D so no random sampling over theta
            r = (random.random()-0.5)*d*self.PIXELS_PER_M
            u_hat = u_hat + r

        return u_hat

    def observe(self, sim_env, x):
        if x == None:
            x = self.x

        self.move_to(x[0], x[1], x[2])
        zs = list()
    
        #FIXME currently just putting object position in LocalizerBot state
        #for obj in sim_env.objects:
            #zs.append(self.project_point(obj.x[0:2]))
        zs.append(self.project_point(x[3:5]))
     
        return np.mat(zs).T



class LaserScanner(Sensor):
    pass


class TouchSensor(Sensor):
    pass

class OpenRAVECamera(Sensor):

    # Creates light-dark environment for localizing robots
    # Measurement decays as 1/(distance squared from beacon)

    NZ = 2 # Depends on number of beacons

    def __init__(self, KK, transform=mat(identity(4))):
        self.KK = KK
        self.transform = transform

    def move_to(self, x):
        #x is a vector: [pos; quaternion]

        pos = x[0:3]
        rot = mat(openravepy.rotationMatrixFromQuat(x[3:7]))
        transform = mat(identity(4))
        transform[0:3,3] = pos
        transform[0:3,0:3] = rot
        self.transform = transform

    def compute_extrinsics(self):
        E = mat(identity(4))
        E[0:3,0:3] = self.transform[0:3,0:3].T
        E[0:3,3] = -E[0:3,0:3] * self.transform[0:3,3]
        return E

    def project_point(self, pt):
        pt_world_coord = mat(ones((4,1)))
        pt_world_coord[0:3] = pt
        E = self.compute_extrinsics()
        XXc = E * pt_world_coord

      
        xn = XXc[0,0]/XXc[2,0]
        yn = XXc[1,0]/XXc[2,0]

        xd = xn
        yd = yn

        xy1d = mat(array([xd, yd, 1.0])).T
        xy1p = self.KK * xy1d
        px = xy1p[0,0]
        py = xy1p[1,0]

        return (px, py)

    def observe(self, sim_env, x):
        if x == None:
            print 'what??'
            x = self.x

        self.move_to(x[0:7])
        zs = list()
    
        #FIXME currently just putting object position in LocalizerBot state
        #for obj in sim_env.objects:
            #zs.append(self.project_point(obj.x[0:2]))
        zs.append(self.project_point(x[7:10]))
     
        return np.mat(zs).T

    def draw(self):
        pass

############################################################
#NOTE Classes below are currently don't fit into framework #
############################################################

'''
Classes below probably not going to be used--use OpenRAVE environment
instead
'''
"""
class PinholeCamera3D(Sensor):

    #PIXELS_PER_M = 10000.0 # Varies of course #FIXME
    PIXELS_PER_M = 1

    def __init__(self, f, w, h):
        self.__init__(f, array([[0], [0], [0]]), array([0, 0, 0, 1]), w, h)

    def __init__(self, f, t, q, w, h):
        self.f = f
        self.translation = matrix(t) # [x; y; z]
        self.orientation = q # Quaternion
        self.width = w
        self.height = h
        self.compute_intrinsics()
        self.compute_extrinsics()
        self.index = PinholeCamera3D.increment_index()

    def __str__(self):
        return 'pinhole_camera_3d[' + str(self.index) + ']'

    def move(self, t, q):
        self.translation += t
        self.orientation *= q
        self.compute_extrinsics()

    def move_to(self, t, q):
        self.translation = t
        self.orientation = q
        self.compute_extrinsics()

    def view(self, scene):
        w, h = self.width, self.height
        plt.figure()
        plt.axes().set_xlim([-w/2.0, w/2.0])
        #plt.axes().set_aspect('equal')
        plt.title(', '.join([str(self), str(scene)]))
        for obj in scene.objects:
            pt = self.project_point(obj.x) #TODO
            u = pt[0]
            v = pt[1]
            if abs(u) > w/2.0 or abs(v) > h/2.0:
               continue 
            plt.plot(u, v, 'bo');
            plt.text(u, v, obj.index)

    def copy(self):
        pass

    def project_point(self, pt):
        u_hmg = self.intrinsics_matrix*self.extrinsics_matrix*vstack((pt, 1))
        return array([u_hmg[0]/u_hmg[2], u_hmg[1]/u_hmg[2]])

    def compute_intrinsics(self):
        self.intrinsics_matrix = matrix([[self.f, 0, 0], [0, self.f, 0], [0, 0, 1]])

    def compute_extrinsics(self):
        R = (q2R(self.orientation)).T # T = I for rotation matrices
        t = -R*self.translation
        self.extrinsics_matrix = bmat('R t')

    def draw(self, ax):
        PPM = self.PIXELS_PER_M
        xs = array([0,  1,  1, 0,  1, -1, 0, -1, -1, 0, -1, 1])*self.width/PPM
        ys = array([0,  1, -1, 0, -1, -1, 0, -1,  1, 0,  1, 1])*self.height/PPM
        zs = array([0,  1,  1, 0,  1,  1, 0,  1,  1, 0,  1, 1])*self.f/PPM
        xyzs = vstack((xs, ys, zs, ones(shape(zs))))
        xyzs = hstack((q2R(self.orientation), self.translation))*xyzs
        xs = xyzs[0,:]; ys = xyzs[1]; zs = xyzs[2]
        xs = array(xs.tolist()[0]);
        ys = array(ys.tolist()[0]);
        zs = array(zs.tolist()[0]) #FIXME
        verts = [zip(xs,ys,zs)]
        ax.add_collection3d(Poly3DCollection(verts))
        t = self.translation
        ax.text(t[0], t[1], t[2], str(self.index))
"""

