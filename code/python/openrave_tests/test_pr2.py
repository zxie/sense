"""Shows how to get all 6D IK solutions.
"""
from openravepy import *
import numpy, time

def waitrobot(robot):
    while not robot.GetController().IsDone():
        time.sleep(0.01)

env = Environment() # create the environment
env.SetViewer('qtcoin') # start the viewer
env.Load('data/pr2test2.env.xml') # load a scene
robot = env.GetRobots()[0] # get the first robot

manip = robot.SetActiveManipulator('leftarm_torso') # set the manipulator to leftarm
#ikmodel = databases.inversekinematics.InverseKinematicsModel(robot,iktype=IkParameterization.Type.Transform6D)
#if not ikmodel.load():
#    ikmodel.autogenerate()

basemanip = interfaces.BaseManipulation(robot)

with env:
        jointnames = ['l_shoulder_lift_joint','l_elbow_flex_joint','l_wrist_flex_joint','r_shoulder_lift_joint','r_elbow_flex_joint','r_wrist_flex_joint']
        jointidxs = [robot.GetJoint(name).GetDOFIndex() for name in jointnames]
        robot.SetActiveDOFs(jointidxs)
        sol = [1.29023451,-2.32099996,-0.69800004,1.27843491,-2.32100002,-0.69799996]
        #basemanip.MoveActiveJoints(goal=sol)


for i in range(1000):
    with env:
        new_sol = list(sol)
        new_sol[1] = sol[1] + i / 1000.0
        robot.SetDOFValues(new_sol, jointidxs)
        time.sleep(0.001)
    env.UpdatePublishedBodies()
            
waitrobot(robot)
 

"""
with env: # lock environment
    #Tgoal = numpy.array([[0,-1,0,-0.21],[-1,0,0,0.04],[0,0,-1,0.92],[0,0,0,1]])
    #sol = manip.FindIKSolution(Tgoal, IkFilterOptions.CheckEnvCollisions) # get collision-free solution
    sol = numpy.array([ 0.2, 0.10706293, -0.5225914, 2.4, -1.35141339, 0.75164049, -1.02974894, 0.59408255])
    print sol
    print len(sol)
    print manip.GetArmIndices()
"""

#basemanip = interfaces.BaseManipulation(robot)
#robot.SetActiveDOFs(manip.GetArmIndices())
#print robot.GetActiveDOF()
#res = basemanip.MoveActiveJoints(goal=sol)

#    with robot: # save robot state
    #robot.SetDOFValues(sol,manip.GetArmIndices()) # set the current solution
    #Tee = manip.GetEndEffectorTransform()
#env.UpdatePublishedBodies() # allow viewer to update new robot
#time.sleep(10)
    
    #raveLogInfo('Tee is: '+repr(Tee))
