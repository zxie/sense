from openravepy import *
from numpy import *
import scipy
import time
import os
import sys 
import math
up_path = os.path.abspath('..')
sys.path.append(up_path)
import _Getch 
from rave_draw import *
from transforms import unscented_transform

from numpy import * 

env=Environment()
env.Load(os.getcwd() + '/testwamcamera3.env.xml')
env.SetViewer('qtcoin')
robot=env.GetRobots()[0]


collisionChecker = RaveCreateCollisionChecker(env,'pqp')
collisionChecker.SetCollisionOptions(CollisionOptions.Distance)
env.SetCollisionChecker(collisionChecker)
report = CollisionReport()

print env.GetBodies()
floor = env.GetKinBody('floor')


for attachedsensor in robot.GetAttachedSensors():
    if attachedsensor.GetSensor() is not None and attachedsensor.GetSensor().Supports(Sensor.Type.Camera):
        attachedsensor.GetSensor().Configure(Sensor.ConfigureCommand.PowerOn)
        #attachedsensor.GetSensor().Configure(Sensor.ConfigureCommand.RenderDataOn)
        getch = _Getch._Getch()
        t = 0
        pts = []
        pts.append(mat(array([-0.0568, -0.2406, 1.3550, 1])).T)
        pts.append(mat(array([0.02, 0.15, 0.7550, 1])).T)
        pts.append(mat(array([0.02, 0.5, 0.7550, 1])).T)
        pts.append(mat(array([0.02, 0.6, 0.7550, 1])).T)
        pts.append(mat(array([0.1, 0.0,1.1, 1])).T)
        while True:
            try:
                #print attachedsensor.GetSensor().GetTransform()
                #attachedsensor.GetSensor().GetSensorGeometry(Sensor.Type.Camera).transform
                
                link_transform = mat(robot.GetLink('wam4').GetTransform())
                #print 'asf'
                #print attachedsensor.GetTransform()
                camera_rel_transform = attachedsensor.GetRelativeTransform()
                #print link_transform
                """
                camera_rel_rot = mat(matrixFromAxisAngle(math.pi / 2.0  * array([0.0, 1.0, 0.0])))
                camera_rel_rot2 = mat(matrixFromAxisAngle(-math.pi / 2.0  * array([0.0, 1.0, 0.0])))
                camera_rel_rot = camera_rel_rot * camera_rel_rot2
                camera_rel_trans = mat(array([0.1, 0.0, 0.0])).T
                camera_rel_rot[0:3,3] = camera_rel_trans
                """
                #print 'asdf'
                #print camera_rel_rot
                #print link_transform * camera_rel_rot
                #print link_transform * mat(attachedsensor.GetRelativeTransform())

                camera_trans = link_transform * camera_rel_transform

                sensordata = attachedsensor.GetData()
                #print sensordata.transform
                #print camera_trans


                E = mat(sensordata.transform) 
                x = pts[t]

                cov = mat(array([(0.1, 0.03, 0.02), (0.03,0.1,0.05), (0.02,0.05,0.1)]))
           
                #ellipsoid = draw_ellipsoid(mu=x[0:3],Sigma=0.1*cov,std_dev=2,env=env)
                sphere1 = draw_sphere(x[0:3], 0.25, env, colors=array((1.0,1.0,1.0,0.5))) 
                sphere2 = draw_sphere(x[0:3], 0.5, env, colors=array((1.0,1.0,1.0,0.25))) 
                sphere3 = draw_sphere(x[0:3], 1.0, env, colors=array((1.0,1.0,1.0,0.10))) 
                sphere4 = draw_sphere(x[0:3], 2.0, env, colors=array((1.0,1.0,1.0,0.05))) 

                
                #print sensordata.imagedata.shape

                arr_pt1 = array([E[0,3], E[1,3], E[2,3]])
                arr_pt2 = E * mat(array([0.2,0.0,0.0,1.0])).T
                #arr_pt2 = array([arr_pt1[0] + tmp_pt2[0], arr_pt1[1] + tmp_pt2[1], arr_pt1[2] + tmp_pt2[2]])

                #print array([x[0], x[1], x[2]]).ravel()

                pt = env.plot3(points=array([x[0], x[1], x[2]]).ravel(), pointsize=10.0, colors=array([1.0, 1.0, 0.0]))
                #pt2 = env.plot3(points=array([E[0,3], E[1,3], E[2,3]]).ravel(), pointsize=10.0, colors=array([1.0, 1.0, 0.0]))
                arr1 = env.drawarrow(p1=arr_pt1, p2=arr_pt2, linewidth=0.01)
              

                #print I*E

                E[0:3,0:3] = E[0:3,0:3].T
                E[0:3,3] = - E[0:3,0:3] * E[0:3,3]

                #XXc = E[0:3,0:3] * (x[0:3] - E[0:3,3])
                #test = E[0:3,0:3].T * XXc[0:3] + E[0:3,3]

                XXc = E * x 
                #print XXc
                #pt2 = env.plot3(points=array([test[0], test[1], test[2]]).ravel(), pointsize=10.0, colors=array([0.0, 1.0, 0.0]))

                xn = XXc[0]/XXc[2]
                yn = XXc[1]/XXc[2]

                xd = xn
                yd = yn

                KK = sensordata.KK
                #print KK
                #KK[0,0] = 0.01 * KK[0,0]
                #KK[1,1] = 0.01 * KK[1,1]

                xy1d = mat(array([xd, yd, 1])).T
                xy1p = KK * xy1d

                #print xy1p
                px = xy1p[0]
                py = xy1p[1]

                #print px, py
                #scipy.misc.pilutil.imsave('test.png',sensordata.imagedata)
                
                mug = env.GetKinBody('mug2')
                mugTransform = mug.GetTransform()
                mugTransform[0,3] = x[0]
                mugTransform[1,3] = x[1]
                mugTransform[2,3] = x[2]
                mug.SetTransform(mugTransform)

                with env:
                    check = env.CheckCollision(robot, report=report)

                    print 'mindist: ', report.minDistance

                
                c = getch()
                #print c
                if c == 'x':
                    break;
                elif ord(c) == 3 or ord(c) == 4:
                    exit(0)
                elif c == 'n':
                    print c
                    t = t + 1
                    if t >= len(pts):
                        t = 0
                elif c == 'c':
                     with env:
                        val = robot.GetDOFValues()[0]
                        robot.SetDOFValues([val - 0.1],[2])
                elif c == 'v':
                        val = robot.GetDOFValues()[0]
                        robot.SetDOFValues([val + 0.1],[2])
                elif c == 'g':
                     with env:
                        val = robot.GetDOFValues()[1]
                        robot.SetDOFValues([val - 0.1],[1])
                elif c == 'b':
                        val = robot.GetDOFValues()[1]
                        robot.SetDOFValues([val + 0.1],[1])


            except openrave_exception:
                time.sleep(0.2)

