"""Shows how to get all 6D IK solutions.
"""
from openravepy import *
import numpy, time
import struct

def waitrobot(robot):
    while not robot.GetController().IsDone():
        time.sleep(0.01)

env = Environment() # create the environment
#env.SetViewer('qtcoin') # start the viewer
env.Load('data/lab1.env.xml') # load a scene
robot = env.GetRobots()[0] # get the first robot

print dir(robot)

manip = robot.SetActiveManipulator('arm')

#manip = robot.SetActiveManipulator('leftarm_torso') # set the manipulator to leftarm
#ikmodel = databases.inversekinematics.InverseKinematicsModel(robot,iktype=IkParameterization.Type.Transform6D)
#if not ikmodel.load():
#    ikmodel.autogenerate()

basemanip = interfaces.BaseManipulation(robot)

with env:
        jointnames = ['Shoulder_Yaw', 'Shoulder_Pitch', 'Shoulder_Roll', 'Elbow', 'Wrist_Yaw', 'Wrist_Pitch', 'Wrist_Roll']
        jointidxs = [robot.GetJoint(name).GetDOFIndex() for name in jointnames]
        print jointidxs
        robot.SetActiveDOFs(jointidxs)
        sol = [0] * len(jointnames)
        #basemanip.MoveActiveJoints(goal=sol)

handles = [ ]
for i in range(1000):
    with env:
        new_sol = list(sol)
        new_sol[3] = sol[3] + i / 1000.0
        robot.SetDOFValues(new_sol, jointidxs)
        #time.sleep(0.001)
        transform = manip.GetEndEffectorTransform()
        xyz = transform[0:3, 3]
        handles.append(env.plot3(points=xyz, pointsize=1.0, colors=numpy.array([1.0, 0.0,0.0])))
        print xyz
    env.UpdatePublishedBodies()
            
waitrobot(robot)


"""
with env: # lock environment
    #Tgoal = numpy.array([[0,-1,0,-0.21],[-1,0,0,0.04],[0,0,-1,0.92],[0,0,0,1]])
    #sol = manip.FindIKSolution(Tgoal, IkFilterOptions.CheckEnvCollisions) # get collision-free solution
    sol = numpy.array([ 0.2, 0.10706293, -0.5225914, 2.4, -1.35141339, 0.75164049, -1.02974894, 0.59408255])
    print sol
    print len(sol)
    print manip.GetArmIndices()
"""

#basemanip = interfaces.BaseManipulation(robot)
#robot.SetActiveDOFs(manip.GetArmIndices())
#print robot.GetActiveDOF()
#res = basemanip.MoveActiveJoints(goal=sol)

#    with robot: # save robot state
    #robot.SetDOFValues(sol,manip.GetArmIndices()) # set the current solution
    #Tee = manip.GetEndEffectorTransform()
#env.UpdatePublishedBodies() # allow viewer to update new robot
#time.sleep(10)
    
    #raveLogInfo('Tee is: '+repr(Tee))
