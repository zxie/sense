from openravepy import *
import time
env = Environment() # create openrave environment
env.SetViewer('qtcoin') # attach viewer (optional)
env.Load('data/lab1.env.xml') # load a simple scene
robot = env.GetRobots()[0] # get the first robot

with env: # lock the environment since robot will be used
  raveLogInfo("Robot "+robot.GetName()+" has "+repr(robot.GetDOF())+" joints with values:\\n"+repr(robot.GetDOFValues()))
  robot.SetDOFValues([1.5],[0]) # set joint 0 to value 0.5
  env.UpdatePublishedBodies()
  time.sleep(1)
  robot.SetDOFValues([1.5],[1]) # set joint 0 to value 0.5
  env.UpdatePublishedBodies()
  time.sleep(1)
  robot.SetDOFValues([1.5],[3]) # set joint 0 to value 0.5
  T = robot.GetLinks()[1].GetTransform() # get the transform of link 1
  env.UpdatePublishedBodies()
  raveLogInfo("The transformation of link 1 is:\n"+repr(T))
