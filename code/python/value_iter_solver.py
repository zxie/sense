import numpy as np
from kalman_filter import linearize
from covar import vec2cov, cov2vec

#Efficient Approximate Value Iteration for Continuous Gaussian POMDPs

class ValueIterationSolver(): 

  def __init__(self, sim_env, M, N, goal_x):
    self.sim_env = sim_env
    self.robot = sim_env.robots[0]
    self.M_model = M
    self.N_model = N
    self.goal_x = goal_x

  def M(self, x_t, u_t):
    return self.M_model

  def N(self, x_t):
    return self.N_model

  def W(self, x_t, Sigma_t, u_t):
    #implements W[x_t, Sigma_t, u_t] = K_t * H_t * Gamma_t

    x_t = np.mat(x_t).T
    Sigma_t = np.mat(Sigma_t)
    u_t = np.mat(u_t).T
    
    A_t, B_t, c_t = self.robot.linearize_dynamics(x_t, u_t)
    h_t, H_t = linearize(lambda x: self.robot.observe(self.sim_env, x=x), x_t, Sigma_t)
    H_t = np.mat(H_t)
    Gamma_t = A_t * Sigma_t * A_t.T + self.M(x_t, u_t)
    K_t = Gamma_t * H_t.T * (H_t * Gamma_t * H_t.T + self.N(x_t)).I

    return K_t * H_t * Gamma_t

  def Phi(self, x_t, Sigma_t, u_t):
    # implements Phi[x_t, Sigma_t, u_t] = Gamma_t - K_t * H_t * Gamma_t
    
    x_t = np.mat(x_t).T
    Sigma_t = np.mat(Sigma_t)
    u_t = np.mat(u_t).T
    
    A_t, B_t, c_t = self.robot.linearize_dynamics(x_t, u_t)
    h_t, H_t = linearize(lambda x: self.robot.observe(self.sim_env, x=x), x_t, Sigma_t)
    H_t = np.mat(H_t)
    Gamma_t = A_t * Sigma_t * A_t.T + self.M(x_t, u_t)
    K_t = Gamma_t * H_t.T * (H_t * Gamma_t * H_t.T + self.N(x_t)).I

    return Gamma_t - K_t * H_t * Gamma_t

  def vecW (self, x_t, vecSigma_t, u_t):
    # implements vecW taking in a vector representation of Sigma_t
    Sigma_t = vec2cov(vecSigma_t)
    W = self.W(x_t, Sigma_t, u_t)
    return cov2vec(W)

  def vecPhi (self, x_t, vecSigma_t, u_t):
    # implements vecPhi, taking in an vector representation of Sigma_t
    Sigma_t = vec2cov(vecSigma_t)
    phi = self.Phi(x_t, Sigma_t, u_t)
    return cov2vec(phi)

  def cost_last(self, x_last, Sigma_last):
    return np.linalg.norm(x_last - self.goal_x.T) + 10 * np.trace(Sigma_last) 

  def cost_t(self, x_t, Sigma_t, u_t):
    return 0.01 * np.linalg.norm(u_t) + np.trace(Sigma_t) 
  
  def solve(self, X_bar, Sigma_bar, U_bar, goal_x):
    T = X_bar.shape[1]
    self.goal_x = goal_x 

    for t in range(T-1):
      print self.W(X_bar[:,t], Sigma_bar[:,:,t], U_bar[:,t])
      print self.vecW(X_bar[:,t], cov2vec(Sigma_bar[:,:,t]), U_bar[:,t])
      print self.vecPhi(X_bar[:,t], cov2vec(Sigma_bar[:,:,t]), U_bar[:,t])
      print self.Phi(X_bar[:,t], Sigma_bar[:,:,t], U_bar[:,t])
      print self.cost_last(X_bar[:,t], Sigma_bar[:,:,t])
      print self.cost_t(X_bar[:,t], Sigma_bar[:,:,t], U_bar[:,t])
    pass






