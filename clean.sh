for d in $(ls -R | grep '/'); do
    cd ${d:2:-1}
    rm *~
    rm *.pyc
    cd ..
done
